-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: db_imara
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_activity`
--

DROP TABLE IF EXISTS `tb_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `judul` varchar(128) DEFAULT NULL,
  `content` mediumtext,
  `cover_image` varchar(128) DEFAULT NULL,
  `tags` varchar(45) DEFAULT NULL,
  `publish` tinyint(1) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_kategori` (`id_kategori`),
  CONSTRAINT `tb_activity_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_activity_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `tb_kategori_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_activity`
--

LOCK TABLES `tb_activity` WRITE;
/*!40000 ALTER TABLE `tb_activity` DISABLE KEYS */;
INSERT INTO `tb_activity` VALUES (1,NULL,1,'coba1','adlhasldjashljdl jkj gkhg kgh khg khg kgk hf khf khf khfk **gkhgkhgkhgkhgkhgh**\r\n','2.-Den-Hasan.jpg','asd,qwe,zxc',1,'2017-10-20 00:52:46','2017-10-19 22:52:33'),(2,NULL,2,'The standard Lorem Ipsum passage, used since the 1500s','**The standard Lorem Ipsum passage, used since the 1500s**\r\n\r\n\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exe*rcitation ullamco laboris nisi ut aliquip *ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"\r\n\r\n**Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC**\r\n\r\n\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor si*t amet, consectetur, adipisci velit, sed quia non *numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"\r\n\r\n**Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC**\r\n\r\n\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"','2.-Den-Hasan.jpg','asd,poi,lkj',1,'2017-10-20 04:00:52','2017-10-20 07:57:17');
/*!40000 ALTER TABLE `tb_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_berita`
--

DROP TABLE IF EXISTS `tb_berita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_berita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `judul` varchar(128) DEFAULT NULL,
  `content` mediumtext,
  `cover_image` varchar(128) DEFAULT NULL,
  `tags` varchar(45) DEFAULT NULL,
  `publish` tinyint(1) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_kategori` (`id_kategori`),
  CONSTRAINT `tb_berita_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_berita_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `tb_kategori_berita` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_berita`
--

LOCK TABLES `tb_berita` WRITE;
/*!40000 ALTER TABLE `tb_berita` DISABLE KEYS */;
INSERT INTO `tb_berita` VALUES (25,NULL,NULL,'Coba 1','aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo \r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Screenshot_from_2017-10-03_06-49-38.png','tags1,tags2,tags3',1,'2017-10-05 23:23:31','2017-10-05 08:15:41'),(26,NULL,2,'Coba 2','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','DSC08053.JPG','tags2,tags4',1,'2017-10-10 16:12:32','2017-10-05 08:19:26'),(27,NULL,1,'Coba 3','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?','Screenshot-2017-9-23_https_www_mathworks_com.png','Hshsh,Sbbs,Hsh,tags4',1,'2017-10-10 18:45:47','2017-10-05 08:50:03'),(28,NULL,2,'Coba 4','est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo \r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritaaspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam tis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','screencapture-partaiponsel-org-toko-1506472605270.png','sda,sdas,tags4',1,'2017-10-10 16:11:13','2017-10-06 06:41:57'),(29,NULL,NULL,'Coba 6','est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo \r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritaaspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam tis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo \r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore verita','20171003_150458.jpg','das,asd,swe',1,'2017-10-10 16:10:37','2017-10-06 06:42:51'),(30,NULL,2,'Coba 5','quae ab illo inventore veritaaspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam tis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nest, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo \r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa ','111232949-5a4fbeb8f8de785f152e49b0ca3d35af.jpg','tag',1,'2017-10-10 18:41:05','2017-10-06 06:43:37'),(31,NULL,2,'Coba 7','quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi u\r\nquae ab illo inventore veritaaspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam tis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nest, qui dolorem ipsum t aliquip ex ea commodo \r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa ','ibu-620x330.jpg','tag',1,'2017-10-10 16:07:25','2017-10-06 06:45:28'),(48,NULL,2,'Apaan','# hasldkhlsakd\r\n* asdasdas\r\n* fadfadffdag\r\n* dgdagad\r\n* dgdaga\r\n# akfd;fknad;knn;\r\n1. flklkaldkflsdg\r\n2. dgdlgjljhdljga\r\n3. dljhjdlgjad\r\n4. djdfjhdjlfaj','4.-Serunya-bermain-kertas-lipat.jpg','tag',0,'2017-10-10 16:06:31','2017-10-06 09:07:28'),(49,NULL,2,'Mbajal','**hsjskbbdj** sadasdasd \r\n# asdas\r\n','IMG_20171008_063814_685.jpg','tag',1,'2017-10-10 18:37:35','2017-10-06 09:09:07'),(50,NULL,1,'Lunyu','hdlashldjalsj j ldjfalsjfhljas','20171003_150458.jpg','tag,tags1',1,'2017-10-19 14:34:32','2017-10-09 14:10:13'),(51,NULL,4,'coba coba','jlsgdlasjgljals','parallax-1.jpg','tatag',1,'2017-10-20 04:03:35','2017-10-11 01:43:48');
/*!40000 ALTER TABLE `tb_berita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_campaign`
--

DROP TABLE IF EXISTS `tb_campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(128) DEFAULT NULL,
  `description` mediumtext,
  `goal_donation` int(11) DEFAULT NULL,
  `raised_donation` int(11) DEFAULT NULL,
  `foto` varchar(128) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_campaign`
--

LOCK TABLES `tb_campaign` WRITE;
/*!40000 ALTER TABLE `tb_campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_kategori_activity`
--

DROP TABLE IF EXISTS `tb_kategori_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_kategori_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_kategori_activity`
--

LOCK TABLES `tb_kategori_activity` WRITE;
/*!40000 ALTER TABLE `tb_kategori_activity` DISABLE KEYS */;
INSERT INTO `tb_kategori_activity` VALUES (1,'kategori A'),(2,'Kategori C'),(3,'Kategori B');
/*!40000 ALTER TABLE `tb_kategori_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_kategori_berita`
--

DROP TABLE IF EXISTS `tb_kategori_berita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_kategori_berita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_kategori_berita`
--

LOCK TABLES `tb_kategori_berita` WRITE;
/*!40000 ALTER TABLE `tb_kategori_berita` DISABLE KEYS */;
INSERT INTO `tb_kategori_berita` VALUES (1,'xxx'),(2,'www'),(4,'zzz');
/*!40000 ALTER TABLE `tb_kategori_berita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_people_say`
--

DROP TABLE IF EXISTS `tb_people_say`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_people_say` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) DEFAULT NULL,
  `jabatan` varchar(128) DEFAULT NULL,
  `quote` mediumtext,
  `foto` varchar(128) DEFAULT NULL,
  `show_quote` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_people_say`
--

LOCK TABLES `tb_people_say` WRITE;
/*!40000 ALTER TABLE `tb_people_say` DISABLE KEYS */;
INSERT INTO `tb_people_say` VALUES (5,'Hanief','Ustad','Sentuh Masa Depan Dengan Belajar','Screenshot_from_2017-07-18_19-56-00.png',1,'2017-09-21 11:12:15'),(7,'Andika','Vokalis','Usu ei porro deleniti similique, per no consetetur necessitatibus. Ut sed augue docendi alienum, ex oblique scaevola inciderint pri, unum movet cu cum. Et cum impedit epicuri','',1,'2017-09-21 11:14:56'),(15,'Ikhsan','Fotografer','foto adalah kenangan yang selalu akan membuat kita ingat tetang masa lalu yang kita ciptakan','Screenshot_from_2017-07-15_07-27-02.png',1,'2017-09-21 14:23:43'),(16,'bbbbbbbb','bbbbbbbbbbb','bbbbbbbbbbbbbbbbbbbbbb','',0,'2017-09-28 06:55:38'),(17,'cccccc','cccccccc','cccccccccccccc','',0,'2017-09-28 06:55:52'),(18,'ddddddd','ddddddd','ddddddddd','',0,'2017-09-28 06:57:53'),(19,'Coba','Owner ','Hidup adalah pilihan ','',1,'2017-10-06 09:06:42');
/*!40000 ALTER TABLE `tb_people_say` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_program`
--

DROP TABLE IF EXISTS `tb_program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(128) DEFAULT NULL,
  `ulasan` mediumtext,
  `cover_img` varchar(128) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_program`
--

LOCK TABLES `tb_program` WRITE;
/*!40000 ALTER TABLE `tb_program` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_setting`
--

DROP TABLE IF EXISTS `tb_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_web` varchar(128) DEFAULT NULL,
  `logo_web` varchar(128) DEFAULT NULL,
  `icon_web` varchar(128) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `telepon` varchar(20) DEFAULT NULL,
  `email` varchar(56) DEFAULT NULL,
  `facebook` varchar(50) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL,
  `instagram` varchar(50) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL,
  `linkedin` varchar(50) DEFAULT NULL,
  `google_plus` varchar(50) DEFAULT NULL,
  `about` text,
  `content_latar_belakang` mediumtext,
  `cover_latar_belakang` varchar(128) DEFAULT NULL,
  `content_tentang_kami` mediumtext,
  `cover_tentang_kami` varchar(128) DEFAULT NULL,
  `visi` varchar(200) DEFAULT NULL,
  `misi` text,
  `parallax_main_text1` varchar(100) DEFAULT NULL,
  `parallax_mini_text1` varchar(200) DEFAULT NULL,
  `parallax_photo1` varchar(128) DEFAULT NULL,
  `parallax_main_text2` varchar(100) DEFAULT NULL,
  `parallax_mini_text2` varchar(200) DEFAULT NULL,
  `parallax_photo2` varchar(128) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_setting`
--

LOCK TABLES `tb_setting` WRITE;
/*!40000 ALTER TABLE `tb_setting` DISABLE KEYS */;
INSERT INTO `tb_setting` VALUES (1,'Imara','LOGO-IMARA.jpg','cropped-Logo-Imara-192x192.png','Watuaji, Keling, Jepara','087567345523','6215836','imara@gmail.com','imara','imara','imara','imara','imara','imara','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repr ehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','banner_2_imara.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Screenshot_from_2017-07-18_19-56-00.png','eeeeeeeeeeeeeeeeeeeeee','1. bbbb bbbbbb bbbb\r\n2. ccc cccc  ccccccccc cccc\r\n3. dddd ddddd dddddd eeeeeee \r\n4. eeeeeeee eeeeeee','Donate Donate Donate','Alfian orangnya jelek nakal gak baik buat tmenan jdjdkdk ddj','4.-Serunya-bermain-kertas-lipat.jpg','BB','dfghjk gggg jgkg khkk gkkg kg kgkgkjg kgk kgkg kgkkjg kgkk kgkjkjgg kkgkk  kjhjhk','4.-Serunya-bermain-kertas-lipat.jpg',1);
/*!40000 ALTER TABLE `tb_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(28) DEFAULT NULL,
  `pass` varchar(128) DEFAULT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `alamat` text,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(56) DEFAULT NULL,
  `handphone` varchar(12) DEFAULT NULL,
  `foto` varchar(128) DEFAULT NULL,
  `konfirmasi` tinyint(1) DEFAULT NULL,
  `status_user` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` VALUES (1,'admin','pbkdf2:sha256:50000$CHi07Yu7$3e7b1a35c404f21df1401a296da49535d6686a867cf154203875293760514e05','Super Admin',NULL,NULL,'admin@mail.com',NULL,NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vw_kategori_activity`
--

DROP TABLE IF EXISTS `vw_kategori_activity`;
/*!50001 DROP VIEW IF EXISTS `vw_kategori_activity`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_kategori_activity` AS SELECT 
 1 AS `id`,
 1 AS `nama_kategori`,
 1 AS `jml`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_kategori_berita`
--

DROP TABLE IF EXISTS `vw_kategori_berita`;
/*!50001 DROP VIEW IF EXISTS `vw_kategori_berita`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_kategori_berita` AS SELECT 
 1 AS `id`,
 1 AS `nama_kategori`,
 1 AS `jml`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_lastPost_activity`
--

DROP TABLE IF EXISTS `vw_lastPost_activity`;
/*!50001 DROP VIEW IF EXISTS `vw_lastPost_activity`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_lastPost_activity` AS SELECT 
 1 AS `id`,
 1 AS `id_user`,
 1 AS `id_kategori`,
 1 AS `judul`,
 1 AS `content`,
 1 AS `cover_image`,
 1 AS `tags`,
 1 AS `publish`,
 1 AS `updated`,
 1 AS `created`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_lastPost_berita`
--

DROP TABLE IF EXISTS `vw_lastPost_berita`;
/*!50001 DROP VIEW IF EXISTS `vw_lastPost_berita`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_lastPost_berita` AS SELECT 
 1 AS `id`,
 1 AS `id_user`,
 1 AS `id_kategori`,
 1 AS `judul`,
 1 AS `content`,
 1 AS `cover_image`,
 1 AS `tags`,
 1 AS `publish`,
 1 AS `updated`,
 1 AS `created`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_tags_activity`
--

DROP TABLE IF EXISTS `vw_tags_activity`;
/*!50001 DROP VIEW IF EXISTS `vw_tags_activity`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_tags_activity` AS SELECT 
 1 AS `tags`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_tags_berita`
--

DROP TABLE IF EXISTS `vw_tags_berita`;
/*!50001 DROP VIEW IF EXISTS `vw_tags_berita`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_tags_berita` AS SELECT 
 1 AS `tags`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'db_imara'
--

--
-- Dumping routines for database 'db_imara'
--

--
-- Final view structure for view `vw_kategori_activity`
--

/*!50001 DROP VIEW IF EXISTS `vw_kategori_activity`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_kategori_activity` AS select `tb_kategori_activity`.`id` AS `id`,`tb_kategori_activity`.`nama_kategori` AS `nama_kategori`,count(`tb_activity`.`id`) AS `jml` from (`tb_kategori_activity` left join `tb_activity` on((`tb_kategori_activity`.`id` = `tb_activity`.`id_kategori`))) where (`tb_activity`.`publish` = 1) group by `tb_kategori_activity`.`nama_kategori` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_kategori_berita`
--

/*!50001 DROP VIEW IF EXISTS `vw_kategori_berita`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_kategori_berita` AS select `tb_kategori_berita`.`id` AS `id`,`tb_kategori_berita`.`nama_kategori` AS `nama_kategori`,count(`tb_berita`.`id`) AS `jml` from (`tb_kategori_berita` left join `tb_berita` on((`tb_kategori_berita`.`id` = `tb_berita`.`id_kategori`))) where (`tb_berita`.`publish` = 1) group by `tb_kategori_berita`.`nama_kategori` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lastPost_activity`
--

/*!50001 DROP VIEW IF EXISTS `vw_lastPost_activity`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_lastPost_activity` AS select `tb_activity`.`id` AS `id`,`tb_activity`.`id_user` AS `id_user`,`tb_activity`.`id_kategori` AS `id_kategori`,`tb_activity`.`judul` AS `judul`,`tb_activity`.`content` AS `content`,`tb_activity`.`cover_image` AS `cover_image`,`tb_activity`.`tags` AS `tags`,`tb_activity`.`publish` AS `publish`,`tb_activity`.`updated` AS `updated`,`tb_activity`.`created` AS `created` from `tb_activity` where (`tb_activity`.`publish` = 1) order by `tb_activity`.`id` desc limit 0,3 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lastPost_berita`
--

/*!50001 DROP VIEW IF EXISTS `vw_lastPost_berita`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_lastPost_berita` AS select `tb_berita`.`id` AS `id`,`tb_berita`.`id_user` AS `id_user`,`tb_berita`.`id_kategori` AS `id_kategori`,`tb_berita`.`judul` AS `judul`,`tb_berita`.`content` AS `content`,`tb_berita`.`cover_image` AS `cover_image`,`tb_berita`.`tags` AS `tags`,`tb_berita`.`publish` AS `publish`,`tb_berita`.`updated` AS `updated`,`tb_berita`.`created` AS `created` from `tb_berita` where (`tb_berita`.`publish` = 1) order by `tb_berita`.`id` desc limit 0,3 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_tags_activity`
--

/*!50001 DROP VIEW IF EXISTS `vw_tags_activity`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_tags_activity` AS select `tb_activity`.`tags` AS `tags` from `tb_activity` where (`tb_activity`.`publish` = 1) order by `tb_activity`.`id` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_tags_berita`
--

/*!50001 DROP VIEW IF EXISTS `vw_tags_berita`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_tags_berita` AS select `tb_berita`.`tags` AS `tags` from `tb_berita` where (`tb_berita`.`publish` = 1) order by `tb_berita`.`id` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-20 14:34:09
