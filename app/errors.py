from app import app
from flask import render_template, request
from modules.libs import data_web

@app.errorhandler(404)
def page_not_found(error):
    dataWeb = data_web()
    app.logger.error('Page not found: %s', (request.path))
    return render_template('error/404.html', data=dataWeb), 404