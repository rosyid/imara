from flask import Flask, redirect, render_template
from flask import g
import os
import json
import MySQLdb
from flaskext.markdown import Markdown

app = Flask(__name__)

BASE = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
FILE_CONFIG = os.path.join(DIR, 'config.json')

try:
    with open(FILE_CONFIG) as config:
        data_json = json.loads(config.read())
except Exception as e:
    raise e

app.secret_key = 'inspirasimasyarakatjepara'

@app.before_request
def db_connect():
    g.conn = MySQLdb.connect(
        host = data_json["db"]["db_host"],
        user = data_json["db"]["db_user"],
        passwd = data_json["db"]["db_password"],
        db = data_json["db"]["db_name"],
        charset = 'utf8',
        use_unicode = True
    )
    g.cursor = g.conn.cursor()

@app.after_request
def db_disconnect(response):
    g.cursor.close()
    g.conn.close()
    return response

from .config import status, STATIC_PATH

if status == 'development':
    app.static_folder = os.path.join(BASE, 'static')
elif status == 'production':
    app.static_folder = STATIC_PATH

Markdown(app)

from urls import *
from errors import *
