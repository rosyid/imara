from flask_script import Command, Manager
from modules.libs import Query
from werkzeug.security import generate_password_hash
import getpass, sys
from app import app, db_connect, db_disconnect

class Admin(Command):
    help = "Create Admin"
    def run(self):
        while True:
            username = raw_input("Username: ")
            if not username:
                print "Masukkan Username!"
                continue
            break
        while True:
            email = raw_input("Email: ")
            if not email:
                print "Masukkan Email!"
                continue
            break
        while True:
            password = getpass.getpass(stream=sys.stderr)
            if not password:
                print "Masukkan Password!"
                continue
            else:
                password2 =  getpass.getpass('Ulangi Password: ')
                if str(password) != str(password2):
                    print "Password Tidak Sama, Ulangi."
                    continue
                break
        
        db_connect()
        query = Query('tb_user')
        results = query.insert({
            "username": username,
            "email": email,
            "nama": "Super Admin",
            "pass": generate_password_hash(str(password)),
            "status_user": 1
        })
        
        print "Super Admin Berhasil Dibuat."
