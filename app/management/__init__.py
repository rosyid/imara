from flask_script import Server, Manager, Shell
from app import app
from .commands.create_admin import Admin

manager = Manager(app, usage="Imara Command Line", with_default_commands=False)
manager.help_args = ('-?', '--help')
manager.add_command('runserver', Server())
manager.add_command('createadmin', Admin())

class Management(object):
    def execute(self):
        manager.run()

def execute_cli():
    manage = Management()
    manage.execute()
