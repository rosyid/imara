from app import BASE, app
import os
import json

DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
FILE_CONFIG = os.path.join(DIR, 'config.json')
ALLOWED_EXTENSIONS = (['png', 'jpg', 'jpeg'])

try:
    with open(FILE_CONFIG) as config:
        data_json = json.loads(config.read())
except Exception as e:
    raise e

status = data_json['status']

if status == "production":
    UPLOAD_FOLDER = data_json["upload_folder"]
    UPLOAD_URL = data_json["upload_url"]
else:
    UPLOAD_FOLDER = os.path.join(BASE, "static/img/uploads")
    UPLOAD_URL = os.path.join("/static/img/uploads")

STATIC_PATH = data_json["static_path"]

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
