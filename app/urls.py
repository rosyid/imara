from app import app

from modules.admin import admin
from modules.blog import blog

app.register_blueprint(admin, url_prefix='/admin')
app.register_blueprint(blog, url_prefix='/')