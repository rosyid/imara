create table tb_user(
id int auto_increment primary key,
username varchar(28),
pass varchar(128),
nama varchar(45),
alamat text,
tgl_lahir date,
email varchar(56),
handphone varchar(12),
foto varchar(128),
konfirmasi tinyint(1),
status_user tinyint(1),
created datetime
);

create table tb_kategori_activity(
id int auto_increment primary key,
nama_kategori varchar(30)
);

create table tb_activity(
id int auto_increment primary key,
id_user int,
id_kategori int,
judul varchar(128),
content mediumtext,
cover_image varchar(128),
tags varchar(45),
publish tinyint(1),
updated timestamp,
created datetime,
foreign key (id_user) references tb_user(id) on delete cascade on update cascade,
foreign key (id_kategori) references tb_kategori_activity(id) on delete cascade on update cascade
);

create table tb_kategori_berita(
id int auto_increment primary key,
nama_kategori varchar(30)
);

create table tb_berita(
id int auto_increment primary key,
id_user int,
id_kategori int,
judul varchar(128),
content mediumtext,
cover_image varchar(128),
tags varchar(45),
publish tinyint(1),
updated timestamp,
created datetime,
foreign key (id_user) references tb_user(id) on delete cascade on update cascade,
foreign key (id_kategori) references tb_kategori_berita(id) on delete cascade on update cascade
);

create table tb_campaign(
id int auto_increment primary key,
judul varchar(128),
description mediumtext,
goal_donation int,
raised_donation int,
foto varchar(128),
updated timestamp,
created datetime
);

create table tb_program(
id int auto_increment primary key,
judul varchar(128),
ulasan mediumtext,
cover_img varchar(128),
updated timestamp,
created datetime
);

create table tb_people_say(
id int auto_increment primary key,
nama varchar(128),
jabatan varchar(128),
quote mediumtext,
foto varchar(128),
show_quote tinyint,
created datetime
);

create table tb_setting(
id int auto_increment primary key,
nama_web varchar(128),
logo_web varchar(128),
icon_web varchar(128),
alamat varchar(100),
phone varchar(15),
telepon varchar(20),
email varchar(56),
facebook varchar(50),
twitter varchar(50),
instagram varchar(50),
path varchar(50),
linkedin varchar(50),
google_plus varchar(50),
about text,
content_latar_belakang mediumtext,
cover_latar_belakang varchar(128),
content_tentang_kami mediumtext,
cover_tentang_kami varchar(128),
visi varchar(200),
misi text,
parallax_main_text1 varchar(100),
parallax_mini_text1 varchar(200),
parallax_photo1 varchar(128),
parallax_main_text2 varchar(100),
parallax_mini_text2 varchar(200),
parallax_photo2 varchar(128),
status tinyint(1)
);

insert into tb_setting set id=default, 
about="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", 
status=1