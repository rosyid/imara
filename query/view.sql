CREATE VIEW vw_kategori_activity
AS
SELECT tb_kategori_activity.id, tb_kategori_activity.nama_kategori, count(tb_activity.id) as jml FROM tb_kategori_activity left join tb_activity on tb_kategori_activity.id = tb_activity.id_kategori where tb_activity.publish=1 group by tb_kategori_activity.nama_kategori;

CREATE VIEW vw_lastPost_activity
AS
SELECT * FROM db_imara.tb_activity where publish = 1 order by id desc limit 0,3;

CREATE VIEW vw_tags_activity
AS
SELECT tags FROM db_imara.tb_activity where publish = 1 order by id desc;

CREATE VIEW vw_kategori_berita
AS
SELECT tb_kategori_berita.id, tb_kategori_berita.nama_kategori, count(tb_berita.id) as jml FROM tb_kategori_berita left join tb_berita on tb_kategori_berita.id = tb_berita.id_kategori where tb_berita.publish=1 group by tb_kategori_berita.nama_kategori;

CREATE VIEW vw_lastPost_berita
AS
SELECT * FROM db_imara.tb_berita where publish = 1 order by id desc limit 0,3;

CREATE VIEW vw_tags_berita
AS
SELECT tags FROM db_imara.tb_berita where publish = 1 order by id desc