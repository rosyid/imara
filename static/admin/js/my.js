    function settingAbout(){
        var dataForm = new FormData();
        var about = document.getElementById('about-input');
        console.log(about.value);
        dataForm.append('about', (about.value == '' || about.value.length == 0) ? '':about.value);        
        $.ajaxSetup({
            // Disable caching of AJAX responses
            cache: false
        });
        $('#btn_simpan_about').html('Menyimpan...');
        $.ajax({
            url: "{{url_for('admin.set_about')}}",
            type: "POST",
            data: dataForm,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                if (data.res == 'berhasil') {
                    $('#aboutAlert').html('<div class="alert alert-success text-center alert-dismissable" id="alertBerhasil"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Berhasil!!</strong> Data Berhasil di Perbarui</div>');
                } else {
                    $('#aboutAlert').html('<div class="alert alert-danger text-center alert-dismissable" id="alertGagal"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Gagal!!</strong> Data Tidak Berhasil di Perbarui</div>');
                }
                $('#btn_simpan_about').html('Simpan');
            }
        });
    };    
    function settingVimi(){
        var dataForm = new FormData();
        var visi = document.getElementById('visi');
        var misi = document.getElementById('misi');
        dataForm.append('visi', (visi.value == '' || visi.value.length == 0) ? '':visi.value);
        dataForm.append('misi', (misi.value == '' || misi.value.length == 0) ? '':misi.value);        
        $.ajaxSetup({
            // Disable caching of AJAX responses
            cache: false
        });
        $('#btn_simpan_vimi').html('Menyimpan...');
        $.ajax({
            url: "{{url_for('admin.set_vimi')}}",
            type: "POST",
            data: dataForm,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                if (data.res == 'berhasil') {
                    $('#vimiAlert').html('<div class="alert alert-success text-center alert-dismissable" id="alertBerhasil"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Berhasil!!</strong> Data Berhasil di Perbarui</div>');
                } else {
                    $('#vimiAlert').html('<div class="alert alert-danger text-center alert-dismissable" id="alertGagal"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Gagal!!</strong> Data Tidak Berhasil di Perbarui</div>');
                }
                $('#btn_simpan_vimi').html('Simpan');
            }
        });
    };
    function settingLatarBelakang(){
        var dataForm = new FormData();
        var konten = document.getElementById('latar_belakang');
        dataForm.append('konten', (konten.value == '' || konten.value.length == 0) ? '':konten.value);       
        $.ajaxSetup({
            // Disable caching of AJAX responses
            cache: false
        });
        $('#btn_simpan_latar_belakang').html('Menyimpan...');
        $.ajax({
            url: "{{url_for('admin.set_content_latar_belakang')}}",
            type: "POST",
            data: dataForm,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                if (data.res == 'berhasil') {
                    $('#latarBelakangAlert').html('<div class="alert alert-success text-center alert-dismissable" id="alertBerhasil"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Berhasil!!</strong> Konten Latar Belakang Diperbarui.</div>');
                } else {
                    $('#latarBelakangAlert').html('<div class="alert alert-danger text-center alert-dismissable" id="alertGagal"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Gagal!!</strong> Konten Latar Belakang Gagal Diperbarui.</div>');
                }
                $('#btn_simpan_latar_belakang').html('Simpan');
            }
        });
    };
    var loadCoverLatarBelakang = function(event){
        var dataForm = new FormData();
        var viewImage = document.getElementById('cover-latar-belakang');
        viewImage.src = '{{url_for('static', filename='img/cbp-loading-popup.gif')}}';

        dataForm.append('image', $('#cover_latar_belakang')[0].files[0]);

        $.ajax({
            url:"{{url_for('admin.upload_cover_latar_belakang')}}",
            type: "POST",
            data: dataForm,
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                console.log(data);                
                if (data.res == 'berhasil') {                    
                    $('#latarBelakangAlert').html('<div class="alert alert-success text-center alert-dismissable" id="alertBerhasil"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Berhasil!</strong> Gambar Icon Berhasil Diperbarui!</div>');
                    viewImage.src = URL.createObjectURL(event.target.files[0]);
                } else {
                    $('#latarBelakangAlert').html('<div class="alert alert-danger text-center alert-dismissable" id="alertGagal"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Gagal!</strong> Gambar Logo Icon Diperbarui!</div>');
                }
            }
        });
    };
    function settingTentangKami(){
        var dataForm = new FormData();
        var konten = document.getElementById('tentang_kami');
        dataForm.append('konten', (konten.value == '' || konten.value.length == 0) ? '':konten.value);       
        $.ajaxSetup({
            // Disable caching of AJAX responses
            cache: false
        });
        $('#btn_simpan_tentang_kami').html('Menyimpan...');
        $.ajax({
            url: "{{url_for('admin.set_content_tentang_kami')}}",
            type: "POST",
            data: dataForm,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                if (data.res == 'berhasil') {
                    $('#tentangKamiAlert').html('<div class="alert alert-success text-center alert-dismissable" id="alertBerhasil"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Berhasil!!</strong> Konten Tentang Kami Diperbarui.</div>');
                } else {
                    $('#tentangKamiAlert').html('<div class="alert alert-danger text-center alert-dismissable" id="alertGagal"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Gagal!!</strong> Konten Tentang Kami Gagal Diperbarui.</div>');
                }
                $('#btn_simpan_tentang_kami').html('Simpan');
            }
        });
    };
    var loadCoverTentangKami = function(event){
        var dataForm = new FormData();
        var viewImage = document.getElementById('cover-tentang-kami');
        viewImage.src = '{{url_for('static', filename='img/cbp-loading-popup.gif')}}';

        dataForm.append('image', $('#cover_tentang_kami')[0].files[0]);

        $.ajax({
            url:"{{url_for('admin.upload_cover_tentang_kami')}}",
            type: "POST",
            data: dataForm,
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                console.log(data);                
                if (data.res == 'berhasil') {                    
                    $('#tentangKamiAlert').html('<div class="alert alert-success text-center alert-dismissable" id="alertBerhasil"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Berhasil!</strong> Cover Tentang Kami Diperbarui!</div>');
                    viewImage.src = URL.createObjectURL(event.target.files[0]);
                } else {
                    $('#tentangKamiAlert').html('<div class="alert alert-danger text-center alert-dismissable" id="alertGagal"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span><strong>Gagal!</strong> Cover Gagal Tentang Kami Diperbarui!</div>');
                }
            }
        });
    };