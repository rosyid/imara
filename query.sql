create database db_imara;

use db_imara;

drop database db_imara;

create table tb_user(
id int auto_increment primary key,
username varchar(28),
pass varchar(128),
nama varchar(45),
alamat text,
tgl_lahir date,
email varchar(56),
handphone varchar(12),
foto varchar(128),
konfirmasi tinyint(1),
status_user tinyint(1),
created datetime
);

create table tb_kategori_activity(
id int auto_increment primary key,
nama_kategori varchar(30)
);

create table tb_activity(
id int auto_increment primary key,
id_user int,
id_kategori int,
judul varchar(128),
content mediumtext,
cover_image varchar(128),
tags varchar(45),
publish tinyint(1),
updated timestamp,
created datetime,
foreign key (id_user) references tb_user(id) on delete cascade on update cascade,
foreign key (id_kategori) references tb_kategori_activity(id) on delete cascade on update cascade
);

create table tb_kategori_berita(
id int auto_increment primary key,
nama_kategori varchar(30)
);

create table tb_berita(
id int auto_increment primary key,
id_user int,
id_kategori int,
judul varchar(128),
content mediumtext,
cover_image varchar(128),
tags varchar(45),
publish tinyint(1),
updated timestamp,
created datetime,
foreign key (id_user) references tb_user(id) on delete cascade on update cascade,
foreign key (id_kategori) references tb_kategori_berita(id) on delete cascade on update cascade
);

create table tb_campaign(
id int auto_increment primary key,
judul varchar(128),
description mediumtext,
goal_donation int,
raised_donation int,
foto varchar(128),
updated timestamp,
created datetime
);

create table tb_program(
id int auto_increment primary key,
judul varchar(128),
ulasan mediumtext,
cover_img varchar(128),
updated timestamp,
created datetime
);

create table tb_people_say(
id int auto_increment primary key,
nama varchar(128),
jabatan varchar(128),
quote mediumtext,
foto varchar(128),
show_quote tinyint,
created datetime
);

create table tb_setting(
id int auto_increment primary key,
nama_blog varchar(128),
logo_blog varchar(128),
icon_blog varchar(128),
alamat varchar(100),
phone varchar(15),
telepon varchar(20),
email varchar(56),
facebook varchar(50),
twitter varchar(50),
instagram varchar(50),
path varchar(50),
linkedin varchar(50),
google_plus varchar(50),
about text,
content_latar_belakang mediumtext,
cover_latar_belakang varchar(128),
content_tentang_kami mediumtext,
cover_tentang_kami varchar(128),
visi varchar(200),
misi text,
parallax_main_text1 varchar(100),
parallax_mini_text1 varchar(200),
parallax_photo1 varchar(128),
parallax_main_text2 varchar(100),
parallax_mini_text2 varchar(200),
parallax_photo2 varchar(128)
);


insert into tb_setting set id=default, status=1;

insert into tb_setting set id=default, nama_web="nama web", logo_web="logo_web", icon_web="icon_blog", alamat="alamat", phone="phone",
telepon="telepon", email="email", facebook="username facebook", twitter="username twitter", instagram="username instagram", path="username path", linkedin="usernmae linkedin", 
google_plus="username google_plus", about="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cur id non ita fit? Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Sed erat aequius Triarium aliquid de dissensione nostra iudicare. Hoc est non modo cor non habere, sed ne palatum quidem. An, partus ancillae sitne in fructu habendus, disseretur inter principes civitatis, P. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Duo Reges: constructio interrete. Experiamur igitur, inquit, etsi habet haec Stoicorum ratio difficilius quiddam et obscurius. Ergo adhuc, quantum equidem intellego, causa non videtur fuisse mutandi nominis. Haeret in salebra.", content_latar_belakang="content_latar_belakang", cover_latar_belakang="cover_latar_belakang", content_tentang_kami="content_tentang_kami",
visi="visi", misi="misi", parallax_main_text1="parallax_main_text1", parallax_mini_text1="parallax_mini_text1", parallax_photo1="parallax_photo1",
parallax_main_text2="parallax_main_text2", parallax_mini_text2="parallax_mini_text2", parallax_photo2="parallax_photo2", status=1;


delimiter //
create trigger setting_history 
after insert on tb_setting
for each row 
begin
insert into tb_setting set id=default, nama_blog=new.nama_blog, logo_blog=new.logo_blog, icon_blog=new.icon_blog, alamat=new.alamat, phone=new.phone,
telepon=new.telepon, email=new.email, facebook=new.facebook, twitter=new.twitter, instagram=new.instagram, path=new.path, linkedin=new.linkedin, 
google_plus=new.google_plus, about=new.about, content_latar_belakang=new.content_latar_belakang, cover_latar_belakang=new.cover_latar_belakang, content_tentang_kami=new.content_tentang_kami,
visi=new.visi, misi=new.misi, parallax_main_text1=new.parallax_main_text1, parallax_mini_text1=new.parallax_mini_text1, parallax_photo1=new.parallax_photo1,
parallax_main_text2=new.parallax_main_text2, parallax_mini_text2=new.parallax_mini_text2, parallax_photo2=new.parallax_photo2, status=0;
end;//

drop trigger setting_history;

drop database db_imara;

use db_imara;

SELECT * FROM vw_kategori_activity;
CREATE VIEW vw_kategori_activity
AS
SELECT tb_kategori_activity.id, tb_kategori_activity.nama_kategori, count(tb_activity.id) as jml FROM tb_kategori_activity left join tb_activity on tb_kategori_activity.id = tb_activity.id_kategori where tb_activity.publish=1 group by tb_kategori_activity.nama_kategori;

SELECT * FROM vw_lastPost_activity;
CREATE VIEW vw_lastPost_activity
AS
SELECT * FROM db_imara.tb_activity where publish = 1 order by id desc limit 0,3;

SELECT * FROM vw_tags_activity;
CREATE VIEW vw_tags_activity
AS
SELECT tags FROM db_imara.tb_activity where publish = 1 order by id desc;

SELECT * FROM vw_kategori_berita;
CREATE VIEW vw_kategori_berita
AS
SELECT tb_kategori_activity.id, tb_kategori_activity.nama_kategori, count(tb_activity.id) as jml FROM tb_kategori_activity left join tb_activity on tb_kategori_activity.id = tb_activity.id_kategori where tb_activity.publish=1 group by tb_kategori_activity.nama_kategori;

SELECT * FROM vw_lastPost_berita;
CREATE VIEW vw_lastPost_berita
AS
SELECT * FROM db_imara.tb_activity where publish = 1 order by id desc limit 0,3;

SELECT * FROM vw_tags_berita;
CREATE VIEW vw_tags_berita
AS
SELECT tags FROM db_imara.tb_activity where publish = 1 order by id desc;