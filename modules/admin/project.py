from modules.admin import admin
from flask import render_template, session, request, redirect, url_for
import math
from modules.libs import read_session, data_admin, Query, pagination2, pagination

@admin.route('/project', methods=['GET', 'POST'])
@read_session
def project():
    key=""
    user = data_admin(session['user_id'])   
    query = Query('project')      
    results = query.get_by()
    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)

    if request.method == 'POST':
        user = data_admin(session['user_id']) 
        pagesizesIndex = int(request.form['pagesizeIndex'])-1
        currentpage = int(request.form['currentpage'])
        query = Query('project')    
        
        if request.form['search']:
            key = request.form['search']  
            sql = 'SELECT * FROM project where name like "%{key}%";'
            sql = sql.format(key=key)
            results = query.custom_query(sql)
            data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
            return render_template('project.html', user=user, data=data, key=key)
        
        else:     
            results = query.get_by()
            data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
            return render_template('project.html', user=user, data=data, key=key)

    return render_template('project.html', user=user, data=data, key=key)

@admin.route('/kategori-project', methods=['GET', 'POST'])
@read_session
def kategori_project():
    key=""
    user = data_admin(session['user_id']) 
    query = Query('project_category')      
    results = query.get_by()
    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)

    if request.method == 'POST':
        pagesizesIndex = int(request.form['pagesizeIndex'])-1
        currentpage = int(request.form['currentpage'])  
        
        if request.form['search']:
            key = request.form['search']
            sql = 'SELECT * FROM project_category where name like "%{key}%";'
            sql = sql.format(key=key)
            results = query.custom_query(sql)
            data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
            return render_template('kategori_project.html', user=user, data=data, key=key)
        
        else:     
            results = query.get_by()
            data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
            return render_template('kategori_project.html', user=user, data=data, key=key)

    return render_template('kategori_project.html', user=user, data=data, key=key)

@admin.route('/tambah-kategori-project', methods=['POST'])
@read_session
def tambah_kategori_project():
    nama = request.form['nama']
    query = Query('project_category')
    result = query.insert({
        "name":nama
        })
    return redirect(url_for('admin.kategori_project'))
