from modules.admin import admin
from flask import render_template, session, request, redirect, url_for, jsonify, flash
import math
from werkzeug.utils import secure_filename
from app import app
from modules.libs import read_session, Query, pagination2, data_web, timestamp, resize
from app.config import allowed_file, UPLOAD_FOLDER, UPLOAD_URL
from itsdangerous import URLSafeSerializer
import os, shutil



@admin.route('/kategori-activity')
@read_session
def kategoriActivity():
    key=""  
    query = Query('tb_kategori_activity')      
    results = query.custom_get_by()
    dataWeb = data_web()
    pagesizesIndex = request.args.get('pagesizeIndex')
    currentpage = request.args.get('currentpage')

    try:
        if pagesizesIndex is not None and currentpage is not None: 
            pagesizesIndex = int(pagesizesIndex)-1
            currentpage = int(currentpage)

            if request.args.get('search'):
                key = request.args.get('search')
                sql = 'SELECT * FROM db_imara.tb_kategori_activity where nama like "%{key}%" order by id desc;'
                sql = sql.format(key=key)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
                return render_template('admin/kategori_activity.html', data=dataWeb, dataTable=data, key=key)            
            else:     
                results = query.custom_get_by()
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
                return render_template('admin/kategori_activity.html', data=dataWeb, dataTable=data, key=key)
    except Exception as e:
        print e

    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)
    if data['status'] is 'Error':
        return render_template('error/404.html', data=dataWeb)
    return render_template('admin/kategori_activity.html', data=dataWeb, dataTable=data, key=key)

@admin.route('/edit-kategori-activity', methods=['POST'])
@read_session
def editKategoriActivity():
    if request.method == 'POST':
        id = request.form['id']
        nama = request.form['nama']
        result = Query('tb_kategori_activity').update({
            "id": id,
            "nama_kategori": nama
        })
        if result:
            flash("Data Kategori activity Berhasil Diperbarui!", "success")
            return redirect(request.referrer)
        else:
            flash("Data Kategori activity Gagal Diperbarui!", "error")
            return redirect(request.referrer)

@admin.route('/tambah-kategori-activity', methods=['POST'])
@read_session
def tambahKategoriActivity():
    if request.method == 'POST':
        nama = request.form['nama']
        result = Query('tb_kategori_activity').insert({
            "nama_kategori": nama
        })
        if result:
            flash("Data Kategori Activity Berhasil Ditambah!", "success")
            return redirect(url_for('admin.kategoriActivity'))
        else:
            flash("Data Kategori Activity Gagal Ditambah!", "error")
            return redirect(url_for('admin.kategoriActivity'))

@admin.route('/hapus-kategori-activity', methods=['POST'])
@read_session
def hapusKategoriActivity():
    if request.method == 'POST':
        id = request.form['id']
        result = Query('tb_kategori_activity').delete(id=id)
        if result:
            flash("Data Kategori Activity Berhasil Dihapus!", "success")
            return redirect(request.referrer)
        else:
            flash("Data Kategori Activity Gagal Dihapus!", "error")
            return redirect(request.referrer)

@admin.route('/semua-activity')
@read_session
def semuaActivity():
    key=""  
    query = Query('tb_activity')      
    results = query.custom_get_by()
    dataWeb = data_web()
    pagesizesIndex = request.args.get('pagesizeIndex')
    currentpage = request.args.get('currentpage')
    token = URLSafeSerializer(app.secret_key)
    print 

    try:
        if pagesizesIndex is not None and currentpage is not None: 
            pagesizesIndex = int(pagesizesIndex)-1
            currentpage = int(currentpage)

            if request.args.get('search'):
                key = request.args.get('search')
                sql = 'SELECT * FROM db_imara.tb_activity where judul like "%{key}%" or tags like "%{key}%" order by created desc;'
                sql = sql.format(key=key)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            else:     
                results = query.custom_get_by()
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            dataActivity = [{
                "token": token.dumps(str(dt[0])),
                "id_user": "" if dt[1] is None else dt[1],
                "id_kategori": "" if dt[2] is None else dt[2],
                "judul": "" if dt[3] is None else dt[3],
                "tags": "" if dt[6] is None else dt[6],
                "publish": "" if dt[7] is None else dt[7],
                "created": str(dt[9])[0:10]
            }for dt in data['results']]
            return render_template('admin/lihat-semua-activity.html', data=dataWeb, dataTable=data, key=key, dataActivity=dataActivity)
    except Exception as e:
        print e

    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)
    if data['status'] is 'Error':
        return render_template('error/404.html', data=dataWeb)
    dataActivity = [{
        "token": token.dumps(str(dt[0])),
        "id_user": "" if dt[1] is None else dt[1],
        "id_kategori": "" if dt[2] is None else dt[2],
        "judul": "" if dt[3] is None else dt[3],
        "tags": "" if dt[6] is None else dt[6],
        "publish": "" if dt[7] is None else dt[7],
        "created": str(dt[9])[0:10]
    }for dt in data['results']]
    return render_template('admin/lihat-semua-activity.html', data=dataWeb, dataTable=data, key=key, dataActivity=dataActivity)

@admin.route('/activity-terbit')
@read_session
def activityTerbit():
    key=""  
    query = Query('tb_activity')      
    results = query.custom_get_by(publish=1)
    dataWeb = data_web()
    pagesizesIndex = request.args.get('pagesizeIndex')
    currentpage = request.args.get('currentpage')
    token = URLSafeSerializer(app.secret_key)
    print 

    try:
        if pagesizesIndex is not None and currentpage is not None: 
            pagesizesIndex = int(pagesizesIndex)-1
            currentpage = int(currentpage)

            if request.args.get('search'):
                key = request.args.get('search')
                sql = 'SELECT * FROM db_imara.tb_activity where publish=1 and (judul like "%{key}%" or tags like "%{key}%") order by created desc;'
                sql = sql.format(key=key)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            else:     
                results = query.custom_get_by(publish=1)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            dataActivity = [{
                "token": token.dumps(str(dt[0])),
                "id_user": "" if dt[1] is None else dt[1],
                "id_kategori": "" if dt[2] is None else dt[2],
                "judul": "" if dt[3] is None else dt[3],
                "tags": "" if dt[6] is None else dt[6],
                "publish": "" if dt[7] is None else dt[7],
                "created": str(dt[9])[0:10]
            }for dt in data['results']]
            return render_template('admin/lihat-activity-terbit.html', data=dataWeb, dataTable=data, key=key, dataActivity=dataActivity)
    except Exception as e:
        print e

    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)
    if data['status'] is 'Error':
        return render_template('error/404.html', data=dataWeb)
    dataActivity = [{
        "token": token.dumps(str(dt[0])),
        "id_user": "" if dt[1] is None else dt[1],
        "id_kategori": "" if dt[2] is None else dt[2],
        "judul": "" if dt[3] is None else dt[3],
        "tags": "" if dt[6] is None else dt[6],
        "publish": "" if dt[7] is None else dt[7],
        "created": str(dt[9])[0:10]
    }for dt in data['results']]
    return render_template('admin/lihat-activity-terbit.html', data=dataWeb, dataTable=data, key=key, dataActivity=dataActivity)

@admin.route('/create-activity')
@read_session
def createActivity():
    create = Query('tb_activity').insert({
        "publish": 0,
        "created": timestamp()
    })

    result = Query('tb_activity').custom_query('SELECT MAX(id) FROM tb_activity;')
    token = URLSafeSerializer(app.secret_key).dumps(str(result[0][0]))
    os.makedirs(os.path.join(os.path.join(UPLOAD_FOLDER, "activity"), token))
    return redirect(url_for('admin.updateActivity', token=token))


@admin.route('/update-activity/<token>', methods=['GET', 'POST'])
@read_session
def updateActivity(token):
    dataWeb = data_web()
    id = URLSafeSerializer(app.secret_key).loads(token)
    dataActivity = [{
        "id": token,
        "id_user": "" if dt[1] is None else dt[1],
        "id_kategori": "" if dt[2] is None else dt[2],
        "judul": "" if dt[3] is None else dt[3],
        "konten": "" if dt[4] is None else dt[4],
        "cover_image": '/static/img/image.png' if dt[5] is None else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'activity'), token), "large_"+dt[5]),
        "tags": "" if dt[6] is None else dt[6],
        "publish": dt[7],
        "created": dt[9]
        }for dt in Query('tb_activity').get_by(id=id)]

    dataKategori = [{
        "id": dt[0],
        "nama": dt[1]
    }for dt in Query('tb_kategori_activity').custom_get_by()]

    if request.method == 'POST':
        token = request.form['token']
        id = URLSafeSerializer(app.secret_key).loads(token)
        judul = request.form['judul']
        konten = request.form['konten']
        publish = request.form['publish']
        tags = request.form['tags']
        id_kategori = request.form['id_kategori']

        try:
            result = Query('tb_activity').update({
                "id":id,
                "id_kategori": id_kategori,
                "judul":judul,
                "content": konten,
                "tags": tags,
                "publish": publish
            })
        except Exception as e:
            print e
        if result:
            flash("Activity Berhasil Diperbarui", "success")
        else:
            flash("Activity Gagal Diperbarui", "error")
        return redirect(url_for('admin.updateActivity', token=token))

    return render_template('admin/tulis_activity.html', data=dataWeb, activity=dataActivity[0], dataKategori=dataKategori)

@admin.route('/upload-cover-activity', methods=['POST'])
@read_session
def uploadCoverActivity():
    if request.method == 'POST':
        file = request.files['coverImage']
        token = request.form['token']
        if file and allowed_file(file.filename):
            direktori = os.path.join(os.path.join(UPLOAD_FOLDER, "activity"), token)
            filename  = secure_filename(file.filename)
            pathnya = os.path.join(direktori, filename)
            file.save(pathnya)
            resize(pathnya, direktori, filename)
            id = URLSafeSerializer(app.secret_key).loads(token)
            oldFoto = Query('tb_activity').get_by(id=id)[0][5]
            os.remove(os.path.join(direktori, "large_"+oldFoto))
            os.remove(os.path.join(direktori, "medium_"+oldFoto))
            os.remove(os.path.join(direktori, "small_"+oldFoto))
            result = Query('tb_activity').update({
                "id": id,
                "cover_image": filename
            })

            if result:
                return jsonify(res="berhasil")
            else:
                return jsonify(res="gagal")
        return jsonify(res="gagal")

@admin.route('/hapus-activity/', methods=['POST'])
@read_session
def hapusActivity():
    if request.method == 'POST':
        token = request.form['token']
        id = URLSafeSerializer(app.secret_key).loads(token)
        try:
            folder_path = os.path.join(os.path.join(UPLOAD_FOLDER, 'activity'), token)
            try:
                shutil.rmtree(folder_path)            
            except Exception as e:
                print e
            result = Query('tb_activity').delete(id=id)
            if result:
                flash('Activity Berhasil Dihapus', 'success')
            else:
                flash('Activity Gagal Dihapus', 'error')
        except Exception as e:
            print e    
        return redirect(request.referrer)