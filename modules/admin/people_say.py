from modules.admin import admin
from flask import render_template, session, request, redirect, url_for, flash
import math
from modules.libs import read_session, data_admin, Query, pagination2, pagination, data_web, timestamp
from werkzeug.utils import secure_filename
from app.config import UPLOAD_FOLDER, UPLOAD_URL
import os


@admin.route('/people-say')
@read_session
def people_say():
    key=""  
    query = Query('tb_people_say')      
    results = query.custom_get_by()
    dataWeb = data_web()
    pagesizesIndex = request.args.get('pagesizeIndex')
    currentpage = request.args.get('currentpage')

    try:
        if pagesizesIndex is not None and currentpage is not None: 
            pagesizesIndex = int(pagesizesIndex)-1
            currentpage = int(currentpage)

            if request.args.get('search'):
                key = request.args.get('search')
                sql = 'SELECT * FROM db_imara.tb_people_say where nama like "%{key}%" or jabatan like "%{key}%" order by created desc;'
                sql = sql.format(key=key)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
                return render_template('admin/people_say.html', data=dataWeb, dataTable=data, key=key)            
            else:     
                results = query.custom_get_by()
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
                return render_template('admin/people_say.html', data=dataWeb, dataTable=data, key=key)
    except Exception as e:
        print e

    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)
    if data['status'] is 'Error':
        return render_template('error/404.html', data=dataWeb)
    return render_template('admin/people_say.html', data=dataWeb, dataTable=data, key=key)

@admin.route('/add-people-say', methods=['GET', 'POST'])
@read_session
def add_people_say():
    if request.method == 'POST':
        foto = request.files['foto']

        if foto.filename != "":
            fotoName = secure_filename(foto.filename)
            foto.save(os.path.join(os.path.join(UPLOAD_FOLDER, 'people_say'), fotoName))

        result = Query('tb_people_say').insert({
            "nama": request.form['nama'],
            "jabatan": request.form['jabatan'],
            "quote": request.form['quote'],
            "foto":"" if foto.filename == "" else fotoName,
            "show_quote": request.form['show'],
            "created": timestamp()
        })
        
        if result:
            flash('Data Berhasil Disimpan', 'success')
            return redirect(url_for('admin.add_people_say'))
        else:
            flash('Data Gagal Disimpan', 'error')
            return redirect(url_for('admin.add_people_say'))
      
    dataWeb = data_web()
    return render_template('admin/add_people_say.html', data=dataWeb)

@admin.route('/people-say/edit/<id>', methods=['GET', 'POST'])
@read_session
def edit_people_say(id):
    if request.method == 'POST':
        foto = request.files['foto']

        if foto.filename != "":
            fotoName = secure_filename(foto.filename)
            foto.save(os.path.join(os.path.join(UPLOAD_FOLDER, 'people_say'), fotoName))
            result = Query('tb_people_say').update({
                "id": id,
                "foto": fotoName,
            })

        result = Query('tb_people_say').update({
            "id": id,
            "nama": request.form['nama'],
            "jabatan": request.form['jabatan'],
            "quote": request.form['quote'],
            "show_quote": request.form['show']
        })
        
        if result:
            flash('Data Berhasil Diperbarui', 'success')
            return redirect(url_for('admin.edit_people_say', id=id))
        else:
            flash('Data Gagal Diperbarui', 'error')
            return redirect(url_for('admin.edit_people_say', id=id))

    result = [{
        "id": dt[0],
        "nama": dt[1],
        "jabatan": dt[2],
        "quote": dt[3],
        "foto": '/static/img/image.png' if dt[4] == "" else os.path.join(os.path.join(UPLOAD_URL, 'people_say'), dt[4]),
        "show_quote": dt[5]
    }for dt in Query('tb_people_say').get_by(id=id)]
    dataWeb = data_web()   
    return render_template('admin/edit_people_say.html', data=dataWeb, data_form=result[0])

@admin.route('/people-say/delete', methods=['POST'])
@read_session
def delete_people_say():
    if request.method == 'POST':         
        try:
            id = request.form['id']
            result = Query('tb_people_say').delete(id)        
            if result:
                flash('Data Berhasil Dihapus.', 'success')
            else:
                flash('Data Gagal Dihapus.', 'Error')
        except Exception as e:
            print e
        return redirect(request.referrer)