from modules.admin import admin
from flask import render_template, session, request
import math
from modules.libs import read_session, data_admin, Query, pagination2, pagination

@admin.route('/user', methods=['GET', 'POST'])
@read_session
def user():
    key=""
    user = data_admin(session['user_id'])   
    query = Query('users')      
    results = query.get_by()
    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)

    if request.method == 'POST':
        user = data_admin(session['user_id']) 
        pagesizesIndex = int(request.form['pagesizeIndex'])-1
        currentpage = int(request.form['currentpage'])
        query = Query('users')   
        
        if request.form['search']:
            key = request.form['search']   
            sql = 'SELECT * FROM users where username like "%{key}%";'
            sql = sql.format(key=key)
            results = query.custom_query(sql)
            data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
            return render_template('user.html', user=user, data=data, key=key)
        
        else:      
            results = query.get_by()
            data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
            return render_template('user.html', user=user, data=data, key=key)

    return render_template('user.html', user=user, data=data, key=key)
