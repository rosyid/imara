from modules.admin import admin
from flask import render_template, session, request, redirect, url_for, jsonify, flash
import math
from werkzeug.utils import secure_filename
from app import app
from modules.libs import read_session, Query, pagination2, data_web, timestamp, resize
from app.config import allowed_file, UPLOAD_FOLDER, UPLOAD_URL
from itsdangerous import URLSafeSerializer
import os, shutil



@admin.route('/kategori-berita')
@read_session
def kategoriBerita():
    key=""  
    query = Query('tb_kategori_berita')      
    results = query.custom_get_by()
    dataWeb = data_web()
    pagesizesIndex = request.args.get('pagesizeIndex')
    currentpage = request.args.get('currentpage')

    try:
        if pagesizesIndex is not None and currentpage is not None: 
            pagesizesIndex = int(pagesizesIndex)-1
            currentpage = int(currentpage)

            if request.args.get('search'):
                key = request.args.get('search')
                sql = 'SELECT * FROM db_imara.tb_kategori_berita where nama like "%{key}%" order by id desc;'
                sql = sql.format(key=key)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
                return render_template('admin/kategori_berita.html', data=dataWeb, dataTable=data, key=key)            
            else:     
                results = query.custom_get_by()
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
                return render_template('admin/kategori_berita.html', data=dataWeb, dataTable=data, key=key)
    except Exception as e:
        print e

    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)
    if data['status'] is 'Error':
        return render_template('error/404.html', data=dataWeb)
    return render_template('admin/kategori_berita.html', data=dataWeb, dataTable=data, key=key)

@admin.route('/edit-kategori-berita', methods=['POST'])
@read_session
def editKategoriBerita():
    if request.method == 'POST':
        id = request.form['id']
        nama = request.form['nama']
        result = Query('tb_kategori_berita').update({
            "id": id,
            "nama_kategori": nama
        })
        if result:
            flash("Data Kategori Berita Berhasil Diperbarui!", "success")
            return redirect(request.referrer)
        else:
            flash("Data Kategori Berita Gagal Diperbarui!", "error")
            return redirect(request.referrer)

@admin.route('/tambah-kategori-berita', methods=['POST'])
@read_session
def tambahKategoriBerita():
    if request.method == 'POST':
        nama = request.form['nama']
        result = Query('tb_kategori_berita').insert({
            "nama_kategori": nama
        })
        if result:
            flash("Data Kategori Berita Berhasil Ditambah!", "success")
            return redirect(url_for('admin.kategoriBerita'))
        else:
            flash("Data Kategori Berita Gagal Ditambah!", "error")
            return redirect(url_for('admin.kategoriBerita'))

@admin.route('/hapus-kategori-berita', methods=['POST'])
@read_session
def hapusKategoriBerita():
    if request.method == 'POST':
        id = request.form['id']
        result = Query('tb_kategori_berita').delete(id=id)
        if result:
            flash("Data Kategori Berita Berhasil Dihapus!", "success")
            return redirect(request.referrer)
        else:
            flash("Data Kategori Berita Gagal Dihapus!", "error")
            return redirect(request.referrer)

@admin.route('/semua-berita')
@read_session
def semuaBerita():
    key=""  
    query = Query('tb_berita')      
    results = query.custom_get_by()
    dataWeb = data_web()
    pagesizesIndex = request.args.get('pagesizeIndex')
    currentpage = request.args.get('currentpage')
    token = URLSafeSerializer(app.secret_key)
    print 

    try:
        if pagesizesIndex is not None and currentpage is not None: 
            pagesizesIndex = int(pagesizesIndex)-1
            currentpage = int(currentpage)

            if request.args.get('search'):
                key = request.args.get('search')
                sql = 'SELECT * FROM db_imara.tb_berita where judul like "%{key}%" or tags like "%{key}%" order by created desc;'
                sql = sql.format(key=key)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            else:     
                results = query.custom_get_by()
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            dataBerita = [{
                "token": token.dumps(str(dt[0])),
                "id_user": "" if dt[1] is None else dt[1],
                "id_kategori": "" if dt[2] is None else dt[2],
                "judul": "" if dt[3] is None else dt[3],
                "tags": "" if dt[6] is None else dt[6],
                "publish": "" if dt[7] is None else dt[7],
                "created": str(dt[9])[0:10]
            }for dt in data['results']]
            return render_template('admin/lihat-semua-berita.html', data=dataWeb, dataTable=data, key=key, dataBerita=dataBerita)
    except Exception as e:
        print e

    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)
    if data['status'] is 'Error':
        return render_template('error/404.html', data=dataWeb)
    dataBerita = [{
        "token": token.dumps(str(dt[0])),
        "id_user": "" if dt[1] is None else dt[1],
        "id_kategori": "" if dt[2] is None else dt[2],
        "judul": "" if dt[3] is None else dt[3],
        "tags": "" if dt[6] is None else dt[6],
        "publish": "" if dt[7] is None else dt[7],
        "created": str(dt[9])[0:10]
    }for dt in data['results']]
    return render_template('admin/lihat-semua-berita.html', data=dataWeb, dataTable=data, key=key, dataBerita=dataBerita)

@admin.route('/berita-terbit')
@read_session
def beritaTerbit():
    key=""  
    query = Query('tb_berita')      
    results = query.custom_get_by(publish=1)
    dataWeb = data_web()
    pagesizesIndex = request.args.get('pagesizeIndex')
    currentpage = request.args.get('currentpage')
    token = URLSafeSerializer(app.secret_key)
    print 

    try:
        if pagesizesIndex is not None and currentpage is not None: 
            pagesizesIndex = int(pagesizesIndex)-1
            currentpage = int(currentpage)

            if request.args.get('search'):
                key = request.args.get('search')
                sql = 'SELECT * FROM db_imara.tb_berita where publish=1 and (judul like "%{key}%" or tags like "%{key}%") order by created desc;'
                sql = sql.format(key=key)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            else:     
                results = query.custom_get_by(publish=1)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            dataBerita = [{
                "token": token.dumps(str(dt[0])),
                "id_user": "" if dt[1] is None else dt[1],
                "id_kategori": "" if dt[2] is None else dt[2],
                "judul": "" if dt[3] is None else dt[3],
                "tags": "" if dt[6] is None else dt[6],
                "publish": "" if dt[7] is None else dt[7],
                "created": str(dt[9])[0:10]
            }for dt in data['results']]
            return render_template('admin/lihat-berita-terbit.html', data=dataWeb, dataTable=data, key=key, dataBerita=dataBerita)
    except Exception as e:
        print e

    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)
    if data['status'] is 'Error':
        return render_template('error/404.html', data=dataWeb)
    dataBerita = [{
        "token": token.dumps(str(dt[0])),
        "id_user": "" if dt[1] is None else dt[1],
        "id_kategori": "" if dt[2] is None else dt[2],
        "judul": "" if dt[3] is None else dt[3],
        "tags": "" if dt[6] is None else dt[6],
        "publish": "" if dt[7] is None else dt[7],
        "created": str(dt[9])[0:10]
    }for dt in data['results']]
    return render_template('admin/lihat-berita-terbit.html', data=dataWeb, dataTable=data, key=key, dataBerita=dataBerita)

@admin.route('/create-berita')
@read_session
def createBerita():
    create = Query('tb_berita').insert({
        "publish": 0,
        "created": timestamp()
    })

    result = Query('tb_berita').custom_query('SELECT MAX(id) FROM tb_berita;')
    token = URLSafeSerializer(app.secret_key).dumps(str(result[0][0]))
    os.makedirs(os.path.join(os.path.join(UPLOAD_FOLDER, "berita"), token))
    return redirect(url_for('admin.updateBerita', token=token))


@admin.route('/update-berita/<token>', methods=['GET', 'POST'])
@read_session
def updateBerita(token):
    dataWeb = data_web()
    id = URLSafeSerializer(app.secret_key).loads(token)
    dataBerita = [{
        "id": token,
        "id_user": "" if dt[1] is None else dt[1],
        "id_kategori": "" if dt[2] is None else dt[2],
        "judul": "" if dt[3] is None else dt[3],
        "konten": "" if dt[4] is None else dt[4],
        "cover_image": '/static/img/image.png' if dt[5] is None else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'berita'), token), "large_"+dt[5]),
        "tags": "" if dt[6] is None else dt[6],
        "publish": dt[7],
        "created": dt[9]
        }for dt in Query('tb_berita').get_by(id=id)]

    dataKategori = [{
        "id": dt[0],
        "nama": dt[1]
    }for dt in Query('tb_kategori_berita').custom_get_by()]

    if request.method == 'POST':
        token = request.form['token']
        id = URLSafeSerializer(app.secret_key).loads(token)
        judul = request.form['judul']
        konten = request.form['konten']
        publish = request.form['publish']
        tags = request.form['tags']
        id_kategori = request.form['id_kategori']

        try:
            result = Query('tb_berita').update({
                "id":id,
                "id_kategori": id_kategori,
                "judul":judul,
                "content": konten,
                "tags": tags,
                "publish": publish
            })
        except Exception as e:
            print e
        if result:
            flash("Berita Berhasil Diperbarui", "success")
        else:
            flash("Berita Gagal Diperbarui", "error")
        return redirect(url_for('admin.updateBerita', token=token))

    return render_template('admin/tulis_berita.html', data=dataWeb, berita=dataBerita[0], dataKategori=dataKategori)

@admin.route('/upload-cover-berita', methods=['POST'])
@read_session
def uploadCoverBerita():
    if request.method == 'POST':
        file = request.files['coverImage']
        token = request.form['token']
        if file and allowed_file(file.filename):
            direktori = os.path.join(os.path.join(UPLOAD_FOLDER, "berita"), token)
            filename  = secure_filename(file.filename)
            pathnya = os.path.join(direktori, filename)
            file.save(pathnya)
            resize(pathnya, direktori, filename)
            id = URLSafeSerializer(app.secret_key).loads(token)
            oldFoto = Query('tb_berita').get_by(id=id)[0][5]
            os.remove(os.path.join(direktori, "large_"+oldFoto))
            os.remove(os.path.join(direktori, "medium_"+oldFoto))
            os.remove(os.path.join(direktori, "small_"+oldFoto))
            result = Query('tb_berita').update({
                "id": id,
                "cover_image": filename
            })

            if result:
                return jsonify(res="berhasil")
            else:
                return jsonify(res="gagal")
        return jsonify(res="gagal")

@admin.route('/hapus-berita/', methods=['POST'])
@read_session
def hapusBerita():
    if request.method == 'POST':
        token = request.form['token']
        id = URLSafeSerializer(app.secret_key).loads(token)
        try:
            folder_path = os.path.join(os.path.join(UPLOAD_FOLDER, 'berita'), token)
            try:
                shutil.rmtree(folder_path)            
            except Exception as e:
                print e
            result = Query('tb_berita').delete(id=id)
            if result:
                flash('Berita Berhasil Dihapus', 'success')
            else:
                flash('Berita Gagal Dihapus', 'error')
        except Exception as e:
            print e    
        return redirect(request.referrer)