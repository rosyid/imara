from modules.admin import admin
import numpy as np
import pandas as pd
from io import BytesIO
from flask import Flask, send_file, Response

import openpyxl
try: 
    from openpyxl.cell import get_column_letter
except ImportError:
    from openpyxl.utils import get_column_letter

import flask_excel as excel
excel.init_excel(admin)

@admin.route('/coba')
def coba():

    #create a random Pandas dataframe
    df_1 = pd.DataFrame(np.random.randint(0,10,size=(10, 4)), columns=list('ABCD'))

    #create an output stream
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')

    #taken from the original question
    df_1.to_excel(writer, startrow = 0, merge_cells = False, sheet_name = "Sheet_1")
    workbook = writer.book
    worksheet = writer.sheets["Sheet_1"]
    format = workbook.add_format()
    format.set_bg_color('#eeeeee')
    worksheet.set_column(0,9,28)

    #the writer has done its job
    writer.close()

    #go back to the beginning of the stream
    output.seek(0)

    #finally return the file
    return send_file(output, attachment_filename="testing.xlsx", as_attachment=True)


@admin.route('/coba2')
def export_xlsx():
    todo_obj={
        "id":2,
        "nama": "aku",
        "alamat": "hehe"
    }
    response = Response(mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=todo.xlsx'
    wb = openpyxl.Workbook()
    ws = wb.get_active_sheet()
    ws.title = "Todo"

    row_num = 0

    columns = [
        (u"sl", 15),
        (u"job", 100),
        (u"Date", 70),
    ]

    for col_num in xrange(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.value = columns[col_num][0]
        # set column width
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]

    for obj in todo_obj:
        row_num += 1
        row = [
            row_num,
            obj.todo_job,
            obj.created_date.strftime("%A %d. %B %Y"),
        ]
        for col_num in xrange(len(row)):
            c = ws.cell(row=row_num + 1, column=col_num + 1)
            c.value = row[col_num]

    wb.save(response)
    return response

@admin.route("/coba3.1", methods=['GET'])
def download_file():
    return excel.make_response_from_array([[1, 2], [3, 4]], "csv")


@admin.route("/coba3.2", methods=['GET'])
def export_records():
    return excel.make_response_from_array([[1, 2], ["sdasd", "qweqw"], ["wdawd","", "dasdas"]], "xlsx", file_name="export_data") 
