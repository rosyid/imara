from flask import Blueprint, render_template, request, session, redirect, url_for, flash, jsonify
from app import app
from app.config import UPLOAD_URL, UPLOAD_FOLDER, allowed_file
from modules.libs import read_session, data_admin
from werkzeug.security import check_password_hash
from werkzeug.utils import secure_filename
from modules.libs import Query, data_web, get_id_setting, hapusFile, custom_resize
import os
from itsdangerous import URLSafeSerializer

admin = Blueprint('admin', __name__)

UPLOAD_FOLDER_WEB = os.path.join(UPLOAD_FOLDER, 'web')

@admin.route('/')
@read_session
def index():
    try:
        dataWeb = data_web()
    except Exception as e:
        print e
    return render_template('admin/index.html', data=dataWeb)

@admin.route('/login', methods=['GET', 'POST'])
def login():
    try:
        try:
            if session['username'] is not None:
                return redirect(url_for('admin.index'))
        except Exception as e:
            pass
        if request.method == 'POST':
            username = request.form['username']
            password = request.form['password']

            query = Query('tb_user')
            results = query.get_by(operand='AND', username=username, status_user=1)
            if len(results) < 1:
                flash('Username Belum Terdaftar', 'username_not_found')
                return redirect(url_for('admin.login'))
            else:
                if check_password_hash(results[0][2], password):
                    session['username'] = results[0][1]
                    session['user_id'] = results[0][0]
                    return redirect(url_for('admin.index'))
                else:
                    flash('Password yang anda masukkan salah', 'password_error')
                    return redirect(url_for('admin.login'))
            print "hahaha"
    except Exception as e:
        flash(e, 'system_error')
        return redirect(url_for('admin.login'))
    dataWeb = data_web()
    return render_template('admin/login.html', data=dataWeb)

@admin.route('/logout')
def logout():
    session.pop('username', None)
    session.pop('user_id', None)
    return redirect(url_for('admin.login'))

@admin.route('/pengaturan/website', methods=['GET', 'POST'])
@read_session
def set_website():
    if request.method == 'POST':
        nama = request.form['nama_web']
        alamat = request.form['alamat']
        phone = request.form['phone']
        telepon = request.form['telepon']
        email = request.form['email']
        facebook = request.form['facebook']
        twitter = request.form['twitter']
        instagram = request.form['instagram']
        path = request.form['path']
        linkedin = request.form['linkedin']
        google_plus = request.form['google_plus']

        data = {
            "id": get_id_setting(),
            "nama_web": nama,
            "alamat": alamat,
            "phone": phone,
            "telepon": telepon,
            "email": email,
            "facebook": facebook,
            "twitter": twitter,
            "instagram": instagram,
            "path": path,
            "linkedin": linkedin,
            "google_plus": google_plus
        }

        result = Query('tb_setting').update(data=data)
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")
        #return redirect(url_for('admin.set_website'))
    dataWeb = data_web()
    return render_template('admin/set_website.html', data=dataWeb)

@admin.route('/pengaturan/website/uploadLogo', methods=['POST'])
@read_session
def upload_logo():
    if request.method == "POST":        
        logo_web = request.files['logo']
        if logo_web and allowed_file(logo_web.filename):
            #hapus file lama
            oldFile = Query('tb_setting').get_by(status=1) #select nama file dari database
            hapusFile(UPLOAD_FOLDER_WEB, oldFile[0][2]) #fungsi menghapus file
            #simpan file baru
            filename = secure_filename(logo_web.filename)
            logo_web.save(os.path.join(UPLOAD_FOLDER_WEB, filename))
            result = Query('tb_setting').update({
                "id": get_id_setting(),
                "logo_web": filename
            })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")
    return jsonify(res="gagal")

@admin.route('/pengaturan/website/uploadIcon', methods=['POST'])
@read_session
def upload_icon():
    if request.method == "POST":
        icon_web = request.files['icon']
        if icon_web and allowed_file(icon_web.filename):
            #hapus file lama
            oldFile = Query('tb_setting').get_by(status=1) #select nama file dari database
            hapusFile(UPLOAD_FOLDER_WEB, oldFile[0][3]) #fungsi menghapus file
            #simpan file baru
            filename = secure_filename(icon_web.filename)
            icon_web.save(os.path.join(UPLOAD_FOLDER_WEB, filename))
            result = Query('tb_setting').update({
                "id": get_id_setting(),
                "icon_web": filename
            })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")
    return jsonify(res="gagal")

@admin.route('/pengaturan/info-website')
@read_session
def set_info_website():
    dataWeb = data_web()
    return render_template('admin/set_info_website.html', data=dataWeb)

@admin.route('/pengaturan/info-website/about', methods=['POST'])
@read_session
def set_about():
    if request.method == "POST":
        about = request.form['about']
        result = Query('tb_setting').update({
            "id": get_id_setting(),
            "about": about
        })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")

@admin.route('/pengaturan/info-website/visi-misi', methods=['POST'])
@read_session
def set_vimi():
    if request.method == "POST":
        visi = request.form['visi']
        misi = request.form['misi']
        result = Query('tb_setting').update({
            "id": get_id_setting(),
            "visi": visi,
            "misi": misi
        })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")

@admin.route('/pengaturan/website/uploadCoverLatarBelakang', methods=['POST'])
@read_session
def upload_cover_latar_belakang():
    if request.method == "POST":
        icon_web = request.files['image']
        if icon_web and allowed_file(icon_web.filename):
            #hapus file lama
            oldFile = Query('tb_setting').get_by(status=1) #select nama file dari database
            hapusFile(UPLOAD_FOLDER_WEB, oldFile[0][16]) #fungsi menghapus file
            #simpan file baru
            filename = secure_filename(icon_web.filename)
            pathnya = os.path.join(UPLOAD_FOLDER_WEB, filename)
            icon_web.save(pathnya)
            custom_resize(pathnya, 940, 940)
            result = Query('tb_setting').update({
                "id": get_id_setting(),
                "cover_latar_belakang": filename
            })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")
    return jsonify(res="gagal")

@admin.route('/pengaturan/info-website/setContentLatarBelakang', methods=['POST'])
@read_session
def set_content_latar_belakang():
    if request.method == "POST":
        konten = request.form['konten']
        result = Query('tb_setting').update({
            "id": get_id_setting(),
            "content_latar_belakang": konten
        })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")

@admin.route('/pengaturan/website/uploadCoverTentangKami', methods=['POST'])
@read_session
def upload_cover_tentang_kami():
    if request.method == "POST":
        icon_web = request.files['image']
        if icon_web and allowed_file(icon_web.filename):
            #hapus file lama
            oldFile = Query('tb_setting').get_by(status=1) #select nama file dari database
            hapusFile(UPLOAD_FOLDER_WEB, oldFile[0][18]) #fungsi menghapus file
            #simpan file baru
            filename = secure_filename(icon_web.filename)
            pathnya = os.path.join(UPLOAD_FOLDER_WEB, filename)
            icon_web.save(pathnya)
            custom_resize(pathnya, 940, 940)
            result = Query('tb_setting').update({
                "id": get_id_setting(),
                "cover_tentang_kami": filename
            })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")
    return jsonify(res="gagal")

@admin.route('/pengaturan/info-website/setContentTentangKami', methods=['POST'])
@read_session
def set_content_tentang_kami():
    if request.method == "POST":
        konten = request.form['konten']
        result = Query('tb_setting').update({
            "id": get_id_setting(),
            "content_tentang_kami": konten
        })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")

@admin.route('/pengaturan/parallax')
@read_session
def parallax():
    dataWeb = data_web()
    return render_template('admin/set_parallax.html', data=dataWeb)

@admin.route('/pengaturan/set-parallax1', methods=['POST'])
@read_session
def parallax1():
    if request.method == 'POST':
        main_text = request.form['main_text1']
        mini_text = request.form['mini_text1']
        result = Query('tb_setting').update({
            "id": get_id_setting(),
            "parallax_main_text1": main_text,
            "parallax_mini_text1": mini_text
        })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")

@admin.route('/pengaturan/upload-photo-parallax1', methods=['POST'])
@read_session
def upload_photo1():
    if request.method == "POST":
        photo = request.files['photo1']
        if photo and allowed_file(photo.filename):
            #hapus file lama
            oldFile = Query('tb_setting').get_by(status=1) #select nama file dari database            
            hapusFile(os.path.join(UPLOAD_FOLDER, 'parallax1'), oldFile[0][23]) #fungsi menghapus file
            #simpan file baru
            filename = secure_filename(photo.filename)
            pathnya = os.path.join(os.path.join(UPLOAD_FOLDER, 'parallax1'), filename)
            photo.save(pathnya)
            custom_resize(pathnya, 1351, 1351)
            result = Query('tb_setting').update({
                "id": get_id_setting(),
                "parallax_photo1": filename
            })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")
    return jsonify(res="gagal")

@admin.route('/pengaturan/set-parallax2', methods=['POST'])
@read_session
def parallax2():
    if request.method == 'POST':
        main_text = request.form['main_text2']
        mini_text = request.form['mini_text2']
        result = Query('tb_setting').update({
            "id": get_id_setting(),
            "parallax_main_text2": main_text,
            "parallax_mini_text2": mini_text
        })
        print result
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")

@admin.route('/pengaturan/upload-photo-parallax2', methods=['POST'])
@read_session
def upload_photo2():
    if request.method == "POST":
        photo = request.files['photo2']
        if photo and allowed_file(photo.filename):
            #hapus file lama
            oldFile = Query('tb_setting').get_by(status=1) #select nama file dari database            
            hapusFile(os.path.join(UPLOAD_FOLDER, 'parallax2'), oldFile[0][26]) #fungsi menghapus file
            #simpan file baru
            filename = secure_filename(photo.filename)
            pathnya = os.path.join(os.path.join(UPLOAD_FOLDER, 'parallax2'), filename)
            photo.save(pathnya)
            custom_resize(pathnya, 1351, 1351)
            result = Query('tb_setting').update({
                "id": get_id_setting(),
                "parallax_photo2": filename
            })
        if result:
            return jsonify(res="berhasil")
        else:
            return jsonify(res="gagal")
    return jsonify(res="gagal")

@admin.route('/setting')
@read_session
def setting():
    user = data_admin(session['user_id']) 
    token = URLSafeSerializer(app.secret_key)
    query = Query('blog')
    data = query.get_by(user_id=session['user_id'])
    data_user = [{
        "username":dt[1],
        "alamat":"" if dt[4] == "None" else dt[4],
        "handphone":"" if dt[5] == "None" else dt[5],
        "nama":"" if dt[12] == "None" else dt[12],
        "website":"" if dt[8] == "None" else dt[8],
        "github":"" if dt[6] == "None" else dt[6],
        "facebook":"" if dt[9] == "None" else dt[9],
        "email":"" if dt[3] == "None" else dt[3],
        "avatar":'/static/img/dev2.png' if dt[14] is None else os.path.join(os.path.join(UPLOAD_URL, '{user_id}/'.format(user_id=token.dumps(session['user_id']))),dt[14])
    } for dt in Query('users').get_by(id=session['user_id'])]
 
    return render_template('setting.html', user=user, data_user=data_user[0])

@admin.route('/settings/profile', methods=['POST'])
@read_session
def update_profile():
    if request.method == 'POST':
        try:
            nama = request.form['nama']
            alamat = request.form['alamat']
            handphone = request.form['handphone']
            github = request.form['github']
            facebook = request.form['facebook']
            website = request.form['website']

            data = {
                "id": session['user_id'],
                "name": nama,
                "address": alamat,
                "handphone": handphone,
                "github": github,
                "facebook": facebook,
                "blog": website
            }

            results = Query('users').update(data=data)
            if results:
                return jsonify(res="berhasil")
            return jsonify(res="gagal")
        except Exception as e:
            print e
            return jsonify(res=str(e))

@admin.route('/settings/profile/upload', methods=['POST'])
@read_session
def avatar_upload():
    if request.method == 'POST':
        images = request.files['file']
        id = session['user_id']
        token = URLSafeSerializer(app.secret_key)
        if images and allowed_file(images.filename):
            if os.path.isdir(os.path.join(UPLOAD_FOLDER, token.dumps(id))) is False:
                os.makedirs(os.path.join(UPLOAD_FOLDER, token.dumps(id)))

            filename = secure_filename(images.filename)
            images.save(os.path.join(os.path.join(UPLOAD_FOLDER, token.dumps(id)), filename))
            token = URLSafeSerializer(app.secret_key)
            query = Query('users')
            results = query.update({
                "id":id,
                "avatar":filename
            })
        return "berhasil"
    return "gagal"

from .people_say import *
from .berita import *
from .activity import *
from .user import *
from .project import *
from .coba import *
