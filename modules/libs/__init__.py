from app import app, db_connect
from functools import wraps
from flask import g, session, redirect, url_for
import os
import time
import datetime
from app.config import UPLOAD_URL, UPLOAD_FOLDER


def sql_column_builder(data=None):
    """
    '''SQL column builder.
from functools import wraps
    A helper for building column list in SQL syntax from python dict.
    Example:

        data = {'id': 1, 'name': 'example'}

        become:

        id='1', name='example'

    Then it can be used with existing SQL syntax by joining them.
    Example:

        sql = 'INSERT INTO table SET {}'.format(sql_column_builder(data))

    '''
    """
    tmp = []
    for key, value in data.iteritems():
        if isinstance(value, basestring):
            tmp.append("{key}='{value}'".format(key=key, value=value))
        else:
            tmp.append("{key}={value}".format(key=key, value=value))

    column_string = ', '.join(tmp)
    return column_string

class Query:
    def __init__(self, table_name=None):
        self.__tablename__=table_name

    def insert(self, data=None):
        """Insert a record.

        :input: data (dict)
        :output: message berisi pesan sukses (dict)
        :error: message berisi pesan error (dict)
        """
        try:
            sql = "INSERT INTO %s SET id=DEFAULT, %s" % (self.__tablename__, sql_column_builder(data))
            #sql = sql.format(table_name=self.__tablename__, column=sql_column_builder(data))
            g.cursor.execute(sql)
            g.conn.commit()
            return True
        except Exception as e:
            return e

    def get_by(self, operand=None, **kwargs):
        try:
            if (len(kwargs) > 0):
                col = {key: value for key, value in kwargs.iteritems()}
                filter=sql_column_builder(col).replace(', ', ' %s ' % (operand))
                sql = 'SELECT * FROM %s WHERE %s' % (self.__tablename__, filter)
                #sql = sql.format(table_name=self.__tablename__, filter=sql_column_builder(col).replace(', ', ' %s ' % (operand)))
            else:
                sql = "SELECT * FROM %s " % (self.__tablename__)
            g.cursor.execute(sql)
            result = g.cursor.fetchall()
            return result
        except Exception as e:
            return e

    def custom_get_by(self, operand=None, **kwargs):
        try:
            if (len(kwargs) > 0):
                col = {key: value for key, value in kwargs.iteritems()}
                filter=sql_column_builder(col).replace(', ', ' %s ' % (operand))
                sql = 'SELECT * FROM %s WHERE %s order by id desc' % (self.__tablename__, filter)
                #sql = sql.format(table_name=self.__tablename__, filter=sql_column_builder(col).replace(', ', ' %s ' % (operand)))
            else:
                sql = "SELECT * FROM %s order by id desc" % (self.__tablename__)
            g.cursor.execute(sql)
            result = g.cursor.fetchall()
            return result
        except Exception as e:
            return e

    def update(self, data=None):
        """ Update Topic.
        :Update: data (dict)
        :output: message berupa pesan sukses (accepted) > dict
        :error: message berupa pesan error (rejected) > dict
        """
        try:
            col = {key: value for key, value in data.iteritems()}
            sql = "UPDATE %s SET %s WHERE id=%s" % (self.__tablename__, sql_column_builder(col), data['id'])
            #sql = sql.format(
            #    table_name=self.__tablename__, column=sql_column_builder(col), id=data['id']
            #)
            g.cursor.execute(sql)
            g.conn.commit()
            return True

        except Exception as e:
            return e
    
    def delete(self, id):
        try:
            sql = 'DELETE FROM %s WHERE id=%s' % (self.__tablename__, id)
            #sql = sql.format(table_name=self.__tablename__, id=id)
            g.cursor.execute(sql)
            g.conn.commit()
            return True
        except Exception as e:
            return e

    def custom_query(self, sql):
        try:
            g.cursor.execute(sql)
            result = g.cursor.fetchall()
            return result
        except Exception as e:
            return e

def read_session(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        session.permanent = True
        try:
            if session['username'] is None:
                return redirect(url_for('admin.login'))
            return f(*args, **kwargs)
        except KeyError:
            return redirect(url_for('admin.login'))
    return wrap

def data_admin(id):
    try:
        query = Query('users')
        result = query.get_by(id=id)
        data = {
            "username": result[0][1]
        }
        return data
    except Exception as e:
        print e
    
def pageNumber(x, y):
    try:
        if (x % y) > 0:
            return (x / y) + 1  
        else:
            return (x / y) 
    except Exception as e:
        print e

def pagination2(data, pagesizes, pagesizesIndex, currentpage):
    try:        
        results = []
        row_number = [0]
        currentpage = currentpage-1
        jml_data = len(data)
        try:
            pagesize = pagesizes[pagesizesIndex]
            pagenumber = int(pageNumber(jml_data, pagesize))
        except Exception as e:
            print e
            
        if pagesizesIndex < 0 or pagesizesIndex > (len(pagesizes)-1) or currentpage >= pagenumber:
            data = {
                "status": "Error",
                "pagesize": "",
                "pagesizes": "",
                "pagenumber": "",
                "currentpage": "",
                "results": "",
                "row_number": "",
                "jml_data": jml_data
                }
            if pagenumber < 1:
                data = {
                    "status": "Not Found",
                    "pagesize": "",
                    "pagesizes": "",
                    "pagenumber": "",
                    "currentpage": "",
                    "results": "",
                    "row_number": "",
                    "jml_data": jml_data
                }
        else:
            for d, dd in enumerate(data):
                if d >= (currentpage * pagesize) and d < ((currentpage+1)*pagesize):
                    results += [dd]
                    row_number += [d+1]
            
            data = {
                "status": "Ok",
                "pagesize": pagesize,
                "pagesizes": pagesizes,
                "pagenumber": pagenumber,
                "currentpage": currentpage+1,
                "results": results,
                "row_number": row_number,
                "jml_data": jml_data
                }
        return data
    except Exception as e:
        print e
    
def pagination(table, pagesizes, pagesizesIndex, currentpage):
    try:
        query = Query()
        row_number=[0]

        pagesize = pagesizes[(pagesizesIndex - 1)]

        sql = 'SELECT count(id) from %s' % (table)
        result = query.custom_query(sql)

        pagenumber = pageNumber(result[0][0], pagesize)

        sql = 'SELECT * FROM %s LIMIT %s, %s' % (table, ((currentpage-1)*pagesize), pagesize)
        results = query.custom_query(sql)

        number = pagesize
        while number > 0:
            number -= 1
            row_number += [(currentpage*pagesize - number)]

        data = {
            "pagesize": pagesize,
            "pagesizes": pagesizes,
            "pagenumber": pagenumber,
            "currentpage": currentpage,
            "results": results,
            "row_number": row_number
            }
        return data
    except Exception as e:
        print e

def data_web():
    try:
        data = [{
            "nama_web":"" if dt[1] is None else dt[1],
            "logo_web":'/static/img/image.png' if dt[2] is None else os.path.join(os.path.join(UPLOAD_URL, 'web'), dt[2]),
            "icon_web":'/static/img/image.png' if dt[3] is None else os.path.join(os.path.join(UPLOAD_URL, 'web'), dt[3]),
            "alamat":"" if dt[4] is None else dt[4],
            "phone":"" if dt[5] is None else dt[5],
            "telepon":"" if dt[6] is None else dt[6],
            "email":"" if dt[7] is None else dt[7],
            "facebook":"" if dt[8] is None else dt[8],
            "twitter":"" if dt[9] is None else dt[9],
            "instagram":"" if dt[10] is None else dt[10],
            "path":"" if dt[11] is None else dt[11],
            "linkedin":"" if dt[12] is None else dt[12],
            "google_plus":"" if dt[13] is None else dt[13],
            "about":"" if dt[14] is None else dt[14],
            "content_latar_belakang":"" if dt[15] is None else dt[15],
            "cover_latar_belakang":'/static/img/image.png' if dt[16] is None else os.path.join(os.path.join(UPLOAD_URL, 'web'),dt[16]),
            "content_tentang_kami":"" if dt[17] is None else dt[17],
            "cover_tentang_kami":'/static/img/image.png' if dt[18] is None else os.path.join(os.path.join(UPLOAD_URL, 'web'), dt[18]),
            "visi":"" if dt[19] is None else dt[19],
            "misi":"" if dt[20] is None else dt[20],
            "parallax_main_text1":"" if dt[21] is None else dt[21],
            "parallax_mini_text1":"" if dt[22] is None else dt[22],
            "parallax_photo1":'/static/img/image.png' if dt[23] is None else os.path.join(os.path.join(UPLOAD_URL, 'parallax1'), dt[23]),
            "parallax_main_text2":"" if dt[24] is None else dt[24],
            "parallax_mini_text2":"" if dt[25] is None else dt[25],
            "parallax_photo2":'/static/img/image.png' if dt[26] is None else os.path.join(os.path.join(UPLOAD_URL, 'parallax2'),dt[26])
        } for dt in Query('tb_setting').get_by(status=1)]

        return data[0]
    except Exception as e:
        print e

def get_id_setting():
    result = Query('tb_setting').get_by(status=1)    
    return result[0][0]

def hapusFile(folder, namaFile):
    try:
        os.remove(os.path.join(folder, namaFile))
    except Exception as e:
        print e

def timestamp():
    ts = time.time()
    times = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    return times

from PIL import Image
from resizeimage import resizeimage

def resize(pathImage, pathDir, filename):
    fd_img = open(pathImage, 'r')
    img = Image.open(fd_img)
    img = resizeimage.resize_thumbnail(img, [940, 940])
    img.save(pathDir+'/large_'+filename, img.format)
    img = resizeimage.resize_thumbnail(img, [345, 345])
    img.save(pathDir+'/medium_'+filename, img.format)
    img = resizeimage.resize_thumbnail(img, [65, 65])
    img.save(pathDir+'/small_'+filename, img.format)
    fd_img.close()
    os.remove(pathImage)

def custom_resize(pathImage, wide, height):
    fd_img = open(pathImage, 'r')
    img = Image.open(fd_img)
    img = resizeimage.resize_thumbnail(img, [wide, height])
    img.save(pathImage, img.format)
    fd_img.close()
    os.remove(pathImage)
