from itsdangerous import URLSafeSerializer
from app import app
from modules.libs import Query
import os
from app.config import UPLOAD_URL


def data_berita():
    data = [{
        "token": URLSafeSerializer(app.secret_key).dumps(str(dt[0])),
        "id_user": dt[1],
        "id_kategori": dt[2],
        "judul": dt[3],
        "content": dt[4],
        "cover_image": '/static/img/image.png' if dt[5] == "" else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'berita'), URLSafeSerializer(app.secret_key).dumps(str(dt[0]))), 'medium_'+dt[5]),
        "tags": dt[6],
        "created": dt[9]
    }for dt in Query('tb_berita').custom_get_by(publish=1)]
    return data

def sidebar():
    try:
        kategori = [{
            "id": URLSafeSerializer(app.secret_key).dumps(str(dt[0])),
            "nama":dt[1],
            "jml":dt[2]
        }for dt in Query().custom_query('SELECT * FROM vw_kategori_berita;')]

        lastPost = [{
            "token":URLSafeSerializer(app.secret_key).dumps(str(dt[0])),
            "judul":dt[3],
            "content":dt[4][0:100],
            "gambar":'/static/img/image.png' if dt[5] == "" else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'berita'), URLSafeSerializer(app.secret_key).dumps(str(dt[0]))), 'small_'+dt[5]),
            "created":dt[9]
        }for dt in Query().custom_query('SELECT * FROM vw_lastPost_berita;')]
        
        q = Query('tb_kategori_berita').custom_query('SELECT * FROM vw_tags_berita;')
        tag=[]
        for d in q:
            for dt in d[0].split(','):
                if dt != "":
                    tag = tag + [dt,]        
        from collections import Counter
        sortedTag = sorted(Counter(tag).items(), key=lambda x: x[1], reverse=True) #[ [k,]*v for k,v in C.items()]

        data = {
            "popularTags": sortedTag,
            "kategori": kategori,
            "lastPost": lastPost
        }
        return data
    except Exception as e:
        print e

def data_activity():
    data = [{
        "token": URLSafeSerializer(app.secret_key).dumps(str(dt[0])),
        "id_user": dt[1],
        "id_kategori": dt[2],
        "judul": dt[3],
        "content": dt[4],
        "cover_image": '/static/img/image.png' if dt[5] == "" else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'activity'), URLSafeSerializer(app.secret_key).dumps(str(dt[0]))), 'medium_'+dt[5]),
        "tags": dt[6],
        "created": dt[9]
    }for dt in Query('tb_activity').custom_get_by(publish=1)]
    return data

def activity_sidebar():
    try:
        kategori = [{
            "id": URLSafeSerializer(app.secret_key).dumps(str(dt[0])),
            "nama":dt[1],
            "jml":dt[2]
        }for dt in Query().custom_query('SELECT * FROM vw_kategori_activity;')]

        lastPost = [{
            "token":URLSafeSerializer(app.secret_key).dumps(str(dt[0])),
            "judul":dt[3],
            "content":dt[4][0:100],
            "gambar":'/static/img/image.png' if dt[5] == "" else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'activity'), URLSafeSerializer(app.secret_key).dumps(str(dt[0]))), 'small_'+dt[5]),
            "created":dt[9]
        }for dt in Query().custom_query('SELECT * FROM vw_lastPost_activity;')]
        
        q = Query('tb_kategori_activity').custom_query('SELECT * FROM vw_tags_activity;')
        tag=[]
        for d in q:
            for dt in d[0].split(','):
                if dt != "":
                    tag = tag + [dt,]        
        from collections import Counter
        sortedTag = sorted(Counter(tag).items(), key=lambda x: x[1], reverse=True) #[ [k,]*v for k,v in C.items()]

        data = {
            "popularTags": sortedTag,
            "kategori": kategori,
            "lastPost": lastPost
        }
        return data
    except Exception as e:
        print e