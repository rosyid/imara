from flask import render_template, Blueprint, request, redirect, url_for
from modules.libs import Query, data_web, pagination2
from app.config import UPLOAD_FOLDER, UPLOAD_URL
from app import app
import os
from itsdangerous import URLSafeSerializer
from .libs import data_berita, sidebar, activity_sidebar, data_activity

from flask import Markup

blog = Blueprint('blog', __name__)

@blog.route('/')
def index():
    people_say = [{
        "id":dt[0],
        "nama":dt[1],
        "jabatan":dt[2],
        "quote": dt[3],
        "foto":'/static/img/image.png' if dt[4] == "" else os.path.join(os.path.join(UPLOAD_URL, 'people_say'), dt[4])
    }for dt in Query('tb_people_say').custom_get_by(show_quote=1)]

    var_berita = data_berita()[0:3]
    var_activity = data_activity()[0:3]
    dataWeb = data_web()
    return render_template('index.html', data=dataWeb, people_say=people_say, berita=var_berita, activity=var_activity)

@blog.route('activity')
def activity():
    key="" 
    kat="" 
    tag=""
    query = Query('tb_activity')      
    results = query.custom_get_by(publish=1)
    dataWeb = data_web()
    sideBar = activity_sidebar()
    pagesizesIndex = request.args.get('pagesizeIndex')
    currentpage = request.args.get('currentpage')
    token = URLSafeSerializer(app.secret_key)
    print 

    try:
        if pagesizesIndex is not None and currentpage is not None: 
            pagesizesIndex = int(pagesizesIndex)-1
            currentpage = int(currentpage)

            if request.args.get('search'):
                key = request.args.get('search')
                sql = 'SELECT * FROM db_imara.tb_activity where publish = 1 and (judul like "%{key}%" or content like "%{key}%" or tags like "%{key}%") order by created desc;'
                sql = sql.format(key=key)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            elif request.args.get('kategori'):
                kat = request.args.get('kategori')
                katLoads = URLSafeSerializer(app.secret_key).loads(kat)
                sql = 'SELECT * FROM db_imara.tb_activity where publish = 1 and id_kategori = "{kat}" order by created desc;'
                sql = sql.format(kat=katLoads)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            elif request.args.get('tag'):
                tag = request.args.get('tag')
                sql = 'SELECT * FROM db_imara.tb_activity where publish = 1 and tags like "%{tag}%" order by created desc;'
                sql = sql.format(tag=tag)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            else:     
                results = query.custom_get_by(publish=1)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            dataActivity = [{
                "token": token.dumps(str(dt[0])),
                "id_user": "" if dt[1] is None else dt[1],
                "id_kategori": "" if dt[2] is None else dt[2],
                "judul": "" if dt[3] is None else dt[3],
                "content": dt[4],
                "cover_image": '/static/img/image.png' if dt[5] == "" else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'activity'), URLSafeSerializer(app.secret_key).dumps(str(dt[0]))), 'large_'+dt[5]),
                "tags": "" if dt[6] is None else dt[6],
                "publish": "" if dt[7] is None else dt[7],
                "created": str(dt[9])[0:10]
            }for dt in data['results']]
            return render_template('all_activity.html', data=dataWeb, dataTable=data, key=key, kat=kat, tag=tag, dataActivity=dataActivity, sideBar=sideBar)
    except Exception as e:
        print e

    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)
    if data['status'] is 'Error':
        return render_template('error/404.html', data=dataWeb)
    dataActivity = [{
        "token": token.dumps(str(dt[0])),
        "id_user": "" if dt[1] is None else dt[1],
        "id_kategori": "" if dt[2] is None else dt[2],
        "judul": "" if dt[3] is None else dt[3],
        "content": dt[4],
        "cover_image": '/static/img/image.png' if dt[5] == "" else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'activity'), URLSafeSerializer(app.secret_key).dumps(str(dt[0]))), 'large_'+dt[5]),
        "tags": "" if dt[6] is None else dt[6],
        "publish": "" if dt[7] is None else dt[7],
        "created": str(dt[9])[0:10]
    }for dt in data['results']]
    return render_template('all_activity.html', data=dataWeb, dataTable=data, key=key, kat=kat, tag=tag, dataActivity=dataActivity, sideBar=sideBar)

@blog.route('activity/<token>')
def lihatActivity(token):
    id = URLSafeSerializer(app.secret_key).loads(token)
    result = [{
        "token": token,
        "id_user": "" if dt[1] is None else dt[1],
        "id_kategori": "" if dt[2] is None else dt[2],
        "judul": "" if dt[3] is None else dt[3],
        "content": dt[4],
        "cover_image": '/static/img/image.png' if dt[5] == "" else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'activity'), URLSafeSerializer(app.secret_key).dumps(str(dt[0]))), 'large_'+dt[5]),
        "tags": [] if len(dt[6]) < 1 else [{
            "nama":tag
        }for tag in dt[6].split(',')],
        "publish": "" if dt[7] is None else dt[7],
        "created": str(dt[9])[0:10]
    }for dt in Query('tb_activity').get_by(id=id, publish=1, operand='AND')]
    dataWeb = data_web()
    sideBar = activity_sidebar()
    return render_template('lihat_activity.html', data=dataWeb, activity=result[0], sideBar=sideBar)

@blog.route('berita')
def berita():
    key="" 
    kat="" 
    tag=""
    query = Query('tb_berita')      
    results = query.custom_get_by(publish=1)
    dataWeb = data_web()
    sideBar = sidebar()
    pagesizesIndex = request.args.get('pagesizeIndex')
    currentpage = request.args.get('currentpage')
    token = URLSafeSerializer(app.secret_key)
    print 

    try:
        if pagesizesIndex is not None and currentpage is not None: 
            pagesizesIndex = int(pagesizesIndex)-1
            currentpage = int(currentpage)

            if request.args.get('search'):
                key = request.args.get('search')
                sql = 'SELECT * FROM db_imara.tb_berita where publish = 1 and (judul like "%{key}%" or content like "%{key}%" or tags like "%{key}%") order by created desc;'
                sql = sql.format(key=key)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            elif request.args.get('kategori'):
                kat = request.args.get('kategori')
                katLoads = URLSafeSerializer(app.secret_key).loads(kat)
                sql = 'SELECT * FROM db_imara.tb_berita where publish = 1 and id_kategori = "{kat}" order by created desc;'
                sql = sql.format(kat=katLoads)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            elif request.args.get('tag'):
                tag = request.args.get('tag')
                sql = 'SELECT * FROM db_imara.tb_berita where publish = 1 and tags like "%{tag}%" order by created desc;'
                sql = sql.format(tag=tag)
                results = query.custom_query(sql)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            else:     
                results = query.custom_get_by(publish=1)
                data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=pagesizesIndex, currentpage=currentpage)
                
                if data['status'] is 'Error':
                    return render_template('error/404.html', data=dataWeb)
            dataBerita = [{
                "token": token.dumps(str(dt[0])),
                "id_user": "" if dt[1] is None else dt[1],
                "id_kategori": "" if dt[2] is None else dt[2],
                "judul": "" if dt[3] is None else dt[3],
                "content": dt[4],
                "cover_image": '/static/img/image.png' if dt[5] == "" else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'berita'), URLSafeSerializer(app.secret_key).dumps(str(dt[0]))), 'large_'+dt[5]),
                "tags": "" if dt[6] is None else dt[6],
                "publish": "" if dt[7] is None else dt[7],
                "created": str(dt[9])[0:10]
            }for dt in data['results']]
            return render_template('all_berita.html', data=dataWeb, dataTable=data, key=key, kat=kat, tag=tag, dataBerita=dataBerita, sideBar=sideBar)
    except Exception as e:
        print e

    data = pagination2(data=results, pagesizes=[5, 10, 15, 20], pagesizesIndex=0, currentpage=1)
    if data['status'] is 'Error':
        return render_template('error/404.html', data=dataWeb)
    dataBerita = [{
        "token": token.dumps(str(dt[0])),
        "id_user": "" if dt[1] is None else dt[1],
        "id_kategori": "" if dt[2] is None else dt[2],
        "judul": "" if dt[3] is None else dt[3],
        "content": dt[4],
        "cover_image": '/static/img/image.png' if dt[5] == "" else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'berita'), URLSafeSerializer(app.secret_key).dumps(str(dt[0]))), 'large_'+dt[5]),
        "tags": "" if dt[6] is None else dt[6],
        "publish": "" if dt[7] is None else dt[7],
        "created": str(dt[9])[0:10]
    }for dt in data['results']]
    return render_template('all_berita.html', data=dataWeb, dataTable=data, key=key, kat=kat, tag=tag, dataBerita=dataBerita, sideBar=sideBar)

@blog.route('berita/<token>')
def lihatBerita(token):
    id = URLSafeSerializer(app.secret_key).loads(token)
    result = [{
        "token": token,
        "id_user": "" if dt[1] is None else dt[1],
        "id_kategori": "" if dt[2] is None else dt[2],
        "judul": "" if dt[3] is None else dt[3],
        "content": dt[4],
        "cover_image": '/static/img/image.png' if dt[5] == "" else os.path.join(os.path.join(os.path.join(UPLOAD_URL, 'berita'), URLSafeSerializer(app.secret_key).dumps(str(dt[0]))), 'large_'+dt[5]),
        "tags": [] if len(dt[6]) < 1 else [{
            "nama":tag
        }for tag in dt[6].split(',')],
        "publish": "" if dt[7] is None else dt[7],
        "created": str(dt[9])[0:10]
    }for dt in Query('tb_berita').get_by(id=id, publish=1, operand='AND')]
    dataWeb = data_web()
    sideBar = sidebar()
    return render_template('lihat_berita.html', data=dataWeb, berita=result[0], sideBar=sideBar)

@blog.route('tentang-kami')
def tentangKami():
    dataWeb = data_web()
    return render_template('tentang-kami.html', data=dataWeb)


@blog.route('coba')
def coba():
    content = """
**hsjskbbdj** *jsdahj asdo jasldj hals*
~~sakjd;kasj;dkja;skdj~~
# dalskdlaskdl
## kjkaldkjlak
### lklkfksdjlfk
> kjdflksdlfkjsdlkfdsfdlklfksdf
> dskjflksdflkjsdf


| Column 1 | Column 2 | Column 3 |
| -------- | -------- | -------- |
| dadasda    | asdasdasd      | sdafddsfdsf    |    
"""
    #content = Markup(markdown.markdown(content))
    return render_template('coba.html', **locals())

from flask_simplemde import SimpleMDE
app.config['SIMPLEMDE_JS_IIFE'] = True
app.config['SIMPLEMDE_USE_CDN'] = True
SimpleMDE(app)
@blog.route('coba2')
def hello():
    return render_template('hello.html')